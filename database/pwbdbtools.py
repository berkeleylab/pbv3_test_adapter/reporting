import os
import itkdb
import getpass

def get_db_client(enable_httptime=True):
    """
    Return an authenticated `itkdb.Client` object.

    If access codes are not automatically picked up (ie: though enviornmental
    variables), then a prompt is shown to the user.    

    Parameters:
     enable_httptime: Enable the `httptime` module for profiling the DB API (default: True)

    Returns:
     itkdb.Client: Authenticated ITKDB client
    """
    if enable_httptime:
        import httptime
        httptime.enable_logging()

    c = itkdb.Client(save_auth='.auth')
    c.user.authenticate()

    return c

def get_property(properties, code):
    """
    Return property value from properties list returned by
    a query to the database.

    In case of a codeTable property, the code is returned.

    Parameters:
     properties (list of dict's): List containing JSON property object
     code (str): Code of property.

    Returns:
     (any): value of property
    """
    prop=next(filter(lambda prop: prop['code']==code, properties))

    #if 'dataType' in prop.keys() and prop['dataType']=='codeTable':
    #    return next(filter(lambda ct: ct['value']==prop['value'], prop['codeTable']))['code']
    # the above two lines are removed due to DB change in Dec 2023. Caveat: new function is needed in order to for example get "3.2 Prime" instead of "5" for powerboard VERSION. 
    return prop['value']

def parse_pwb_list(pblist):
    """
    The input string is a comma-separated list of Powerboard numbers,
    with ranges (x-y) allowed. Thus function splits then into a list
    and evaluates the ranges.

    For example `1,3-5` returns [1,3,4,5].

    Parameters:
     pblist (string): Comma-separated PWB numbers. Ranges are allowed (ie: 1,3-5).

    Returns:
     list: List of integers corresponding to `pblist`.
    """

    pb_nums=[]
    for pb_num in pblist.split(','):
        if '-' in pb_num:
            parts=pb_num.split('-')
            pb_nums += list(range(int(parts[0]),int(parts[1])+1))
        else:
            pb_nums += [int(pb_num)]

    return pb_nums
    

def create_serial(version,batch,pwb_num):
    """
    Build the ATLAS serial number for a Powerboard given
    production information.

    Parameters:
     version (int): Version code
     batch (int): Batch code
     pwb_num (int): Powerboard number in batch

    Returns:
     string: built serial number
    """

    return "20USBP0{}{:02d}{:04d}".format(version,batch,pwb_num)

def parse_serial(sn, fullsn=True):
    """
    Extract Powerboard production information from a serial number.
    Parameters:
     sn (str): Serial Number
     fullsn (bool): True when `sn` is full serial number, False if 7 least singificant digits only
    Returns:
     version (int), batch (int), number (int)
    """

    if not fullsn and len(sn)!=7:
        return None,None,None
    
    offset  = 7 if fullsn else 0

    version = int(sn[offset])
    batch   = int(sn[offset+1:offset+3])
    number  = int(sn[offset+3:offset+11])
    
    return version,batch,number

def get_random_components_batch(c, batch_code, batch_number, currentLocation='LBNL_STRIP_POWERBOARDS', n=1, filt={}):
    """
    Get unassembled components from batch `batch_number`.
    
    The batches are assumed to contain only a single component type.

    The function throws an error if the batch is not found
    or there are not enough parts available.

    Parameters:
     c (itkdb.Client): Authenticated ITK DB client
     batch_code (str): Batch code.
     batch_number (str): Batch number
     currentLocation (str): Current location of component
     n (int): Number of components to return
     filt (dict): Extra requirements added to filter map

    Return:
     list of str: Codes of unassembled component
    """

    filterMap={'project':'S', 'subproject':subproject, 'componentType':comp_type, 'type':sub_type, 'currentLocation':currentLocation, 'assembled': False}
    filterMap.update(filt)

    comp_list = c.get("listComponents", json = {'filterMap': filterMap} )

    # Need to determine trashed state...
    components=[]
    while len(components)<n:
        comp=next(comp_list)
        comp=c.get('getComponent',json={'component':comp['code']})
        if comp['trashed']: continue
        components.append(comp['code'])

    return components

def get_random_components(c, subproject, comp_type, sub_type, currentLocation='LBNL_STRIP_POWERBOARDS', n=1, filt={}):
    """
    Get unassembled components of type `comp_type`.

    The function throws an error if the component is not found
    or there are not enough parts available.

    Parameters:
     c (itkdb.Client): Authenticated ITK DB client
     subproject (str): Subproject code
     comp_type (str): Component type to look-up
     sub_type (str): Component type type to look-up
     currentLocation (str): Current location of component
     n (int): Number of components to return
     filt (dict): Extra requirements added to filter map

    Return:
     list of str: Codes of unassembled component
    """

    filterMap={'project':'S', 'subproject':subproject, 'componentType':comp_type, 'type':sub_type, 'currentLocation':currentLocation, 'assembled': False, 'trashed': False}
    filterMap.update(filt)

    comp_list = c.get("listComponents", json = {'filterMap': filterMap} )

    # Need to determine trashed state...
    components=[]
    while len(components)<n:
        try:
            comp=next(comp_list)
        except StopIteration:
            raise Exception('Found only {nfound}/{nreq} of required {comp_type}.'.format(nfound=len(components), nreq=n, comp_type=comp_type))
        comp=c.get('getComponent',json={'component':comp['code']})
        if comp['trashed']: continue # Trashed filter does not actually work
        components.append(comp['code'])

    return components

def getLatestTestRunByComponent(client, component, testType, stage=None):
    """
    Get the latest testRun (except the deleted ones) from a component

    Parameters:
    c (itkdb.Client): Authenticated ITK DB client
    component (str): component code
    testType (list of str): list of testType name
    stage (list of str): optional requirement of the stage of the component

    Return:
    a TestRun object if exisit, otherwise return None
    """
    json_={'component':component, 'testType':testType}
    if stage != None:
        json_['stage'] = stage
    testRuns = client.get('listTestRunsByComponent', json=json_)
    testRun = None
    for temp in testRuns:
        if temp['state'] != 'ready':
            continue
        testRun = temp
        break
    return testRun


def registerFlexArray(client, panelid, flex_batch, arr_type='PB_FLEX_ARRAY_3V2'):
    version, batch, number = parse_serial(panelid, fullsn=False)

    regdata = {
        'project':'S',
        'subproject':'SB',
        'institution':'LBNL_STRIP_POWERBOARDS',
        'componentType':'PB_FLEX_ARRAY',
        'type':arr_type,
        'serialNumber':'20USBPF'+panelid,
        'batches':{'PWB_FLEX_BATCH':flex_batch},
        'properties':{'VERSION':str(version), 'BATCH':batch, 'NUMBER':number}
    }

    client.post('registerComponent', json=regdata)
    print("Register of panel "+panelid+" done.")

