#!/usr/bin/env python

import os, sys
import argparse
import itkdb
import random
import tempfile
from datetime import datetime
from PIL import Image

from database import pwbdb
from database import pwbdbtools
from database import pwbdbpandas
from database import transitions

def split_boards(filename, pbid1, pbid2, centered=False):
    """
    Split a picture with two powerboards into two pictures

    Parameters:
    filename (str): filename of the picture
    pbid1 (int): ID of the upper board
    pbid2 (int): ID of the lower board
    centered (bool): if set to be True, then the picture only has one board in the center, in this case only need to give pbid1 in the argument

    Return:
    the filenames of the saved picture after split
    """
    if pbid1 == None:
        pbid1=0
    if pbid2 == None:
        pbid2=0

    fileSuffix = os.path.splitext(filename)[1]
    fileDir = tempfile.mkdtemp()

    img = Image.open(filename)
    width, height = img.size
    filename1 = fileDir+"/"+str(pbid1)+fileSuffix
    filename2 = fileDir+"/"+str(pbid2)+fileSuffix

    img1 = None
    img2 = None
    if centered: #only crop and save the center part
        img1 = img.crop((0, height/4, width, height*3/4))
    else:
        img1 = img.crop((0, 0, width, height/2))
        img2 = img.crop((0, height/2, width, height))

    if pbid1 > 0:
        img1.save(filename1)
    if pbid2 > 0 and (not centered):
        img2.save(filename2)
    return filename1, filename2
    

def uploadpicture(client, filename, pbid, passed, comment):
    """
    upload the picture of a powerboad to database
    
    Parameters:
     client (itkdb.Client): Authenticated ITk DB client
     filename (str): filename of the picture 
     pbid (int): powerboard id on the shieldbox
     passed (bool): whether passed inspection or not
     comment (str): any comment to add regarding this test 
    """
    if pbid == None:
        #by default, the filename is the pbid if pbid is not specified
        pbid = int (os.path.basename(filename).split(".")[0]) 
    print("uploading "+filename+" to powerboard with pbid = "+str(pbid))
    version, batch, pb_num = pwbdbtools.parse_serial(str(pbid), fullsn=False)
    print("version = "+str(version)+", batch = "+str(batch) + ", pb_num = "+str(pb_num))
    
    pfilt=[
        {'code': 'BATCH'     ,'operator': '=','value': batch       },
        {'code': 'PB_NUMBWE' ,'operator': '=','value': pb_num      }
        ]
    r=pwbdbpandas.listComponentsByProperty(client, project='S', componentType='PWB', propertyFilter=pfilt)
    if len(r) < 1:
        raise Exception('Unable to find the powerboard in the database with the provided pbid = '+str(pbid))
    pbs=r[r.VERSION==str(version)]
    powerboard=pwbdb.Component(pbs.iloc[0].code, client=client)
    print("Found the powerboard: code = "+powerboard.code+", serialNumber = "+powerboard.serialNumber+", stage = "+powerboard.currentStage)

    runNumber = "0"
    testRun = pwbdbtools.getLatestTestRunByComponent(client, component=powerboard.code, testType=["PICTURE"], stage=[powerboard.currentStage])
    if testRun != None:
        runNumber = str(int(testRun['runNumber'])+1)
    print("Creating test run of Picture now...")
    client.post("uploadTestRunResults", json={'component': powerboard.code, 'runNumber': runNumber, 'passed': passed, 'date': str(datetime.now().strftime('%d.%m.%Y %H:%M')), 'institution':'LBNL_STRIP_POWERBOARDS', 'testType': "PICTURE", 'results': {"PICTURE": "undefined", "COMMENT": comment}})
    testRun = pwbdbtools.getLatestTestRunByComponent(client, component=powerboard.code, testType=["PICTURE"], stage=[powerboard.currentStage])
    print("Uploading image to testRun: "+testRun['id'])
    client.post("createBinaryTestRunParameter", data=dict(testRun=testRun['id'], parameter="PICTURE"), files={'data': (filename, open(filename, mode='rb'), filename.split(".")[-1])})
    print("Image uploaded, check it here: https://itkpd-test.unicorncollege.cz/testRunView?id="+testRun['id'])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Upload a picture of the powerboard.")

    parser.add_argument("filename",     help="filename of the picture to be uploaded")
    parser.add_argument("--pbid1",      type=int, help="ID on the shieldbox of the upper powerbox (if not specified, the upper powerboard will not be uploaded)")
    parser.add_argument("--pbid2",      type=int, help="ID on the shieldbox of the lower powerbox (if not specified, the lower powerboard will not be uploaded)")
    parser.add_argument("--failed1",    default=False, action="store_true", help="whether or not upper board fail the visual inspection")
    parser.add_argument("--failed2",    default=False, action="store_true", help="whether or not lower board fail the visual inspection")
    parser.add_argument("--comment1",   default="Whole powerboard.", help="comment for upper board")
    parser.add_argument("--comment2",   default="Whole powerboard.", help="comment for lower board")
    parser.add_argument("--centered",    default=False, action="store_true", help="whether or not the provided picture is centered around a single board (if yes, crop the center part and only upload one board with ID of pbid1)")

    args = parser.parse_args()
    c = pwbdbtools.get_db_client()

    filename1, filename2 = split_boards(args.filename, args.pbid1, args.pbid2, args.centered)

    if args.pbid1 != None:
        uploadpicture(c, filename1, args.pbid1, not args.failed1, args.comment1)
        os.remove(filename1)
    if args.pbid2 != None and (not args.centered):
        uploadpicture(c, filename2, args.pbid2, not args.failed2, args.comment2)
        os.remove(filename2)
    os.rmdir(os.path.dirname(filename1))
