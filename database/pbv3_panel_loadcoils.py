#!/usr/bin/env python

import os, sys
import argparse
import itkdb
import random

from database import pwbdb
from database import pwbdbtools
from database import transitions

def soldercoils(client, panelid, currentLocation):
    """
    Assemble coils onto all Powerboards on a panel
    
    Parameters:
     client (itkdb.Client): Authenticated ITk DB client
     panelid (str): Panel ID to load (VBBXXXX)
     currentLocation (str): filter for currentLocation of random components
    """

    print('Solder coils on panel {}'.format(panelid))
    #
    # Find panel component
    panel = transitions.Panel(panelid, client=client)

    # Get necessary amount of random components
    nflex=0
    if len(panel.children['PWB']) == 0:
        nflex=len(panel.children['PB_FLEX'])
        print('Found {} flexes on panel.'.format(nflex))
    else:
        nflex=len(panel.children['PWB'])
        print('Found {} powerboards on panel.'.format(nflex))
    coilsall = pwbdbtools.get_random_components(client, 'SG', 'PWB_COIL', 'COILV3A', currentLocation, n=nflex*2)

    coils = []
    for idx in range(len(coilsall)):
        if len(coils) == nflex:
            break
        coil_this = pwbdb.Component(coilsall[idx], client=client)
        if coil_this.parents is None:
            coils.append(coil_this)
    if len(coils) < nflex:
        print('Error! Can not find enough free coils .... ')
        sys.exit(1)

    # Loop over children
    idx_coil = 0
    if len(panel.children['PWB']) == 0:
        for flex in panel.children['PB_FLEX'].values():
            powerboard=next(filter(lambda p: p.componentType=='PWB', flex.parents))
            if 'PWB_COIL' in powerboard.children:
                print('Coil is already soldered on powerboard '+powerboard.code)
            else:
                coil=coils[idx_coil]
                idx_coil += 1
                print('Assemble coil {} to powerboard {}'.format(coil.code,powerboard.code))
                powerboard.assemble(coil)
    else:
        for powerboard in panel.children['PWB'].values():
            if 'PWB_COIL' in powerboard.children:
                print('Coil is already soldered on powerboard '+powerboard.code)
            else:
                coil=coils[idx_coil]
                idx_coil += 1
                print('Assemble coil {} to powerboard {}'.format(coil.code,powerboard.code))
                powerboard.assemble(coil)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Manually load a Powerboard.")

    parser.add_argument("panelid", help="Panel identifier (VBBXXXX).")
    parser.add_argument("-c","--currentLocation", default = 'LBNL_STRIP_POWERBOARDS', help="Filter random coilss by current location.")

    args = parser.parse_args()

    c = pwbdbtools.get_db_client()

    soldercoils(c, args.panelid, args.currentLocation)
