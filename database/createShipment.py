#!/usr/bin/env python3

import os
import argparse
import itkdb
import getpass

def create_serial(version,batch,pwb_num):
    batch=batch.zfill(2)
    pwb_num=pwb_num.zfill(4)

    return"20USBP0"+version+batch+pwb_num

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Creates shipment of items given their serial numbers.")

    parser.add_argument("recipient", help="Code for recipient institution.")
    parser.add_argument("globe", help="One of domestic, intraContinental, or continental.")
    parser.add_argument("version", help="Powerboard version.")
    parser.add_argument("batch", help="Powerboard batch.")
    parser.add_argument("-nc","--no_cache", action='store_true', help="Run the script without checking the cache.")
    parser.add_argument("-i","--items", nargs='*', help="Powerboard numbers of items in shipment.")

    args = parser.parse_args()

    serials = [create_serial(args.version,args.batch,pwb_num) for pwb_num in args.items]

    global u
    global c

    if os.environ.get('ITKDB_ACCESS_CODE1') and os.environ.get('ITKDB_ACCESS_CODE2'):
      accessCode1 = os.environ.get('ITKDB_ACCESS_CODE1')
      accessCode2 = os.environ.get('ITKDB_ACCESS_CODE2')
    else:
      accessCode1 = getpass.getpass("AccessCode1: ")
      accessCode2 = getpass.getpass("AccessCode2: ")

    u = itkdb.core.User(accessCode1 = accessCode1, accessCode2 = accessCode2)
    c = itkdb.Client(user = u, expires_after=dict(days=1))
    c.user.authenticate()
	
    component_codes = []
    for i in serials:
        try:
            if args.no_cache:
                component = c.get("getComponent", json = {'component':i}, headers={'Cache-Control':'no-cache'})
            else:
                component = c.get("getComponent", json = {'component':i})
            code = component['code']
            component_codes.append(code)
        except Exception as e:
            print(e)
            print("If the powerboard was not found, try running with the --no_cache argument to check the most up to date version of the database.")

    c.post("createShipment", json = {'name':'Powerboards', 'sender':'LBL', 'recipient':args.recipient, 'type':args.globe, 'status':'inTransit','shipmentItems':component_codes})
