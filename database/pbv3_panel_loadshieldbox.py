#!/usr/bin/env python

import os, sys
import argparse
import itkdb
import random
import time

from database import pwbdb
from database import pwbdbtools
from database import pwbdbpandas
from database import transitions
from database import pbv3_shieldbox_mark

def loadshieldbox(client, panelid, position, pb_num, sb_version=None, sb_batch=None):
    """
    Manually load a powerboard with a marked shield box.
    
    Parameters:
     client (itkdb.Client): Authenticated ITk DB client
     panelid (str): Panel ID to load (VBBXXXX)
     position (int): Powerboard position
     pb_num (int) Powerboard number at position
    """

    #
    # Find panel component
    panel = transitions.Panel(panelid, client=client) 
    powerboard = None
    if len(panel.children['PWB']) == 0:
        flex=panel.children['PB_FLEX'][position]
        powerboard=next(filter(lambda p: p.componentType=='PWB', flex.parents))
    else:
        powerboard=panel.children['PWB'][position]
    if len(powerboard.children['PWB_SHIELDBOX'])>0:
        print('Powerboard ({}) already has a shield box!'.format(powerboard.code))
        return 

    # Find the necessary shield box
    version=panel.property('VERSION')
    batch  =panel.property('BATCH')

    # in case the shield box version/batch don't match the panel id...
    if sb_batch is not None:
        batch = sb_batch
    if sb_version is not None:
        version = sb_version

    # mark shield box in case it's not done yet
    print("Mark shield box number for version "+str(version)+", batch "+str(batch)+", pbnum "+str(pb_num))
    pbv3_shieldbox_mark.mark_shield_box(client, "BARRELSHIELDBOX3A", sb_version, sb_batch, str(pb_num))
    time.sleep(5) # wait a bit to make it visible in DB

    # get the shield box that are marked
    pfilt=[
        #{'code': 'VERSION','operator': '=','value': str(version)}, # does not work..
        {'code': 'BATCH'     ,'operator': '=','value': batch       },
        {'code': 'PB_NUMBWE' ,'operator': '=','value': pb_num      }
        ]

    # r=client.get('listComponentsByProperty', json={'project':'S', 'componentType':'PB_FLEX_ARRAY', 'propertyFilter':pfilt})
    # panel=next(r)

    r=pwbdbpandas.listComponentsByProperty(client, project='S', componentType='PWB_SHIELDBOX', propertyFilter=pfilt)
    if len(r.index)==0 or len(r[r.VERSION==str(version)].index)==0:
        raise Exception('Unable to find a shiled box with the corresponding markings.')

    sbs=r[r.VERSION==str(version)]
    sb=pwbdb.Component(sbs.iloc[0].code, client=client)
    if sb.parents is not None:
        raise Exception('Found shield box ({}) already has a parent!'.format(sb.code))

    print('Found a shield box: {}'.format(sb.code))    

    # Assemble :)
    print('Assembling to Powerboard {}'.format(powerboard.code))
    powerboard.assemble(sb)
    powerboard.property('PB_NUMBWE', pb_num)
    powerboard.property('BATCH', batch)
    powerboard.property('VERSION', str(version))

    client.post('updateComponentSN',json={'component':powerboard.code, 'serialNumber':pwbdbtools.create_serial(version, batch, pb_num)})
    time.sleep(5) # wait a bit to make it visible in DB

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Manually load a Powerboard.")

    parser.add_argument("panelid"           , help="Panel identifier (VBBXXXX).")
    parser.add_argument("position", type=int, help="Powerboard position.")
    parser.add_argument("pb_num"  , type=int, help="Powerboard number at given position.")

    args = parser.parse_args()

    c = pwbdbtools.get_db_client()

    loadshieldbox(c, args.panelid, args.position, args.pb_num)
