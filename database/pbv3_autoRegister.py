#!/usr/bin/env python

import os, sys, time
import argparse
import itkdb

from database import pwbdb
from database import pwbdbtools
from database import pwbdbpandas
from database import transitions
from database import pbv3_panel_smdload
from database import pbv3_panel_transition
from database import pbv3_panel_loadcoils
from database import pbv3_panel_loadshieldbox
from database import pbv3_panel_singulate
from database import pbv3_panel_loadhvmux
from database import pbv3_panel_loadamac

import gspread
import glob
import json

"""
    Google sheet API has a quota limit of 60 reads and 60 writes per mininute per user per project. To not exceed that limit, sleep 1 second after every read/write access 
"""

result_dir = '/storage/petalinux/pbv3_mass_tester_2020.3/home/rackop/results'

def getAMACeFuseFromTest(json_path, panelid, version, batch, serial):
    eFuse = -1
    try:
        filesAll = glob.glob(json_path+"/panel"+str(panelid)+"/*/pb20USBP0{:01d}{:02d}{:04d}".format(int(version), int(batch), int(serial))+"/*alive.json")
        if len(filesAll) == 0:
            return eFuse
        file_json = ""
        for file_json_ in filesAll:
            if os.path.getsize(file_json_) > 10:
                file_json = file_json_
                break
        if file_json == "":
            return eFuse
        jdata = json.load(open(file_json))
        for test in jdata['tests']:
            if test['testType'] == 'PADID':
                eFuse = test['results']['eFuse']
    except Exception:
        print(" Could not find eFuse from test for this board")
    return eFuse

def get_amac_by_eFuse(client, eFuse):
    chips=pwbdbpandas.listComponentsByProperty(client, 'S',
                                               componentType='AMAC',
                                               propertyFilter=[
                                                   {'code':'AMAC_EFUSE' ,'operator':'=','value':eFuse},
                                               ])

    ## Check whether something was found
    if len(chips.index) != 1:
        raise Exception('Returned non-1 AMACv2\'s with the corresponding wafer identification.')
    chip=chips[chips.AMAC_EFUSE==eFuse]

    if len(chip.index)==0:
        raise Exception('Unable to find an AMACv2 with the corresponding wafer identification.')

    chip_code = chip.iloc[0].code
    chip=pwbdb.Component(chip_code, client=client)
    return chip

def updateSheet(sheet, irow, panel, panelid, client, checkTests = False, checkAMAC = False):
    cell_ready = sheet.find("Ready for DB register?")
    time.sleep(1)
    cell_currentStage = sheet.find("Stage in DB")
    time.sleep(1)
    cell_currentLocation = sheet.find("Current Location")
    time.sleep(1)
    cell_shipmentDestination = sheet.find("Shipment destination")
    time.sleep(1)
    cell_passTests = sheet.find("#PBs pass all tests")
    time.sleep(1)
    cell_dbLink = sheet.find("DB link")
    time.sleep(1)

    if cell_ready is None or cell_currentStage is None or cell_currentLocation is None or cell_shipmentDestination is None or cell_passTests is None or cell_dbLink is None:
        return 

    dbLink = "https://itkpd-test.unicorncollege.cz/componentView?code="+panel['code']
    currentLocation = panel['currentLocation']['name']
    currentStage = panel['currentStage']['name']
    shipmentDestination = ""
    if panel['shipmentDestination'] is not None:
        shipmentDestination = panel['shipmentDestination']['name']
    componentType = panel['componentType']['code']
    print("Current location: " + currentLocation)
    print("Shipment destination: " + shipmentDestination)
    print("Current stage: " + currentStage)
    print("Link to itkpd page: " + dbLink)

    if currentLocation != sheet.cell(irow, cell_currentLocation.col).value:
        sheet.update_cell(irow, cell_currentLocation.col, currentLocation)
    time.sleep(2)

    if currentStage != sheet.cell(irow, cell_currentStage.col).value:
        sheet.update_cell(irow, cell_currentStage.col, currentStage)
    time.sleep(2)

    if shipmentDestination != sheet.cell(irow, cell_shipmentDestination.col).value:
        sheet.update_cell(irow, cell_shipmentDestination.col, shipmentDestination)
    time.sleep(2)

    if dbLink != sheet.cell(irow, cell_dbLink.col).value:
        sheet.update_cell(irow, cell_dbLink.col, dbLink)
    time.sleep(2)

    # test check 
    if checkTests:
        panel_transition = transitions.Panel(panelid, client)
        transition = transitions.available[panel['currentStage']['code']]
        pass_test = transition.check_tests(panel_transition, ignore_tests=["STATUS", "PICTURE", "HV_ENABLE", "VISUAL_INSPECTION", "HVSENSE", "DCDCEFFICIENCY", "VIN"])
        print("#PB Pass tests: "+str(pass_test))

        if pass_test != sheet.cell(irow, cell_passTests.col).value:
            sheet.update_cell(irow, cell_passTests.col, pass_test)
        time.sleep(2)

    # check AMAC
    if checkAMAC:
        cell_checkAMAC = sheet.find("AMAC die check result")
        time.sleep(2)
        if cell_checkAMAC is None:
            return 

        checkAMAC_exist = sheet.cell(irow, cell_checkAMAC.col).value
        time.sleep(2)

        if checkAMAC_exist is None:
            print("Checking AMAC die numbers on panel {}".format(panelid))
            panel_ = transitions.Panel(panelid, client)
            correct_dies = ""
            allCorrect = True
            checked = True
            for position in range(10):
                try:
                    pwb=panel_.children['PWB'][position]
                    
                    if len(pwb.children['AMAC'])>0:
                        amac = pwb.children['AMAC'][0]
                        eFuse_db = amac.property('AMAC_EFUSE')
                        die_db = amac.property('AMACNUMBER')
                        pb_num = pwb.property('PB_NUMBWE')
                        pb_batch = pwb.property('BATCH')
                        pb_version = pwb.property('VERSION')
                        eFuse_json = getAMACeFuseFromTest(result_dir, panelid, pb_version, pb_batch, pb_num)
                        if eFuse_json == -1:
                            print("No test result yet for Panelid = "+str(panelid)+", pb "+str(position))
                            checked = False
                            correct_dies += str(die_db)+","
                            continue # no test result yet
                        if eFuse_db != eFuse_json:
                            amac_new = get_amac_by_eFuse(client, eFuse_json)
                            die_new = amac_new.property('AMACNUMBER')
                            allCorrect = False
                            correct_dies += str(die_new)+","
                            print("Wrong AMAC in database! Panelid = "+str(panelid)+", pb "+str(position)+", die/eFuse on db = "+str(die_db)+"/"+str(eFuse_db) + ", die/eFuse from electrical tests  = "+str(die_new)+"/"+str(eFuse_json))
                            print(amac_new.code)

                            print("=== Disassemble old amac "+amac.code+" from PWB "+pwb.code)
                            pwb.disassemble(amac)
                            time.sleep(2)
                            parents_new = amac_new.parents
                            if parents_new is not None and len(parents_new)>0:
                                for parent_new in parents_new:
                                    parent_new_type = parent_new.dump['componentType']['code']
                                    print("AMAC with eFuse "+str(eFuse_json) + " already assembled on "+parent_new_type+" : "+parent_new.code)
                                    print("=== Disassemble new amac ({}) from its current parent {} ({})".format(amac_new.code, parent_new_type, parent_new.code))
                                    parent_new.disassemble(amac_new)
                                    time.sleep(2)
                            print("=== Assemble new amac "+amac_new.code+" to PWB "+pwb.code)
                            pwb.assemble(amac_new)
                            time.sleep(2)
                        else:
                            correct_dies += str(die_db)+","
                            print("Correct AMAC in database for Panelid = "+str(panelid)+", pb "+str(position))
                    else:
                        print('Powerboard ({}) does not have an AMACv2 yet'.format(pwb.code))
                        pb_num = pwb.property('PB_NUMBWE')
                        pb_batch = pwb.property('BATCH')
                        pb_version = pwb.property('VERSION')
                        eFuse_json = getAMACeFuseFromTest(result_dir, panelid, pb_version, pb_batch, pb_num)
                        if eFuse_json == -1:
                            checked = False
                            print('Powerboard ({}) does not have a test result yet'.format(pwb.code))
                            continue
                        amac_new = get_amac_by_eFuse(client, eFuse_json)
                        die_new = amac_new.property('AMACNUMBER')
                        allCorrect = False
                        correct_dies += str(die_new)+","
                        print("eFuse/die from electrical test: "+str(eFuse_json)+"/"+str(die_new)+", code:"+amac_new.code)
                        parents_new = amac_new.parents
                        if parents_new is not None and len(parents_new)>0:
                            for parent_new in parents_new:
                                parent_new_type = parent_new.dump['componentType']['code']
                                print("AMAC with eFuse "+str(eFuse_json) + " already assembled on "+parent_new_type+" : "+parent_new.code)
                                print("=== Disassemble new amac ({}) from its current parent {} ({})".format(amac_new.code, parent_new_type, parent_new.code))
                                parent_new.disassemble(amac_new)
                                time.sleep(2)
                        print("=== Assemble new amac "+amac_new.code+" to PWB "+pwb.code)
                        pwb.assemble(amac_new)
                        time.sleep(2)

                except Exception:
                    print("...")

            die_check_result = ""
            if checked:
                if allCorrect:
                    die_check_result += "All correct."
                else:
                    die_check_result += "Updated. Correct dies: "+correct_dies[:-1]
                sheet.update_cell(irow, cell_checkAMAC.col, die_check_result)
                time.sleep(2)
            print("Check result: "+die_check_result)
        else:
            print("checkAMAC already done: "+checkAMAC_exist)


            
def updatePanel(sheet, irow, client, checkTests = False, checkAMAC = False, flex_array_batch="2021-12-01", carrier_card_batch = "20220112", bpol_type='BPOL12V4'):

    panelid = sheet.cell(irow, 1).value
    time.sleep(1)
    #panel = transitions.Panel(panelid, client)
    print("Panle ID: "+panelid)
    panel = None
    try:
        panel = client.get('getComponent', json={'component':panelid, 'alternativeIdentifier':True})
    except Exception:
        print('Carrier card of ' + panelid +' not found in DB..')

    if panel is None:
        # check if flex array exists:
        flexArray = None
        try:
            flexArray = client.get('getComponent', json={'component':'20USBPF'+panelid})
        except Exception:
            print('Flex array of '+panelid+' not found in DB')

        if flexArray is None: 
            print('Register powerboard flex array for '+panelid+" now...")
            pwbdbtools.registerFlexArray(client, panelid, flex_array_batch, arr_type='PB_FLEX_ARRAY_3V2')
            flexArray = client.get('getComponent', json={'component':'20USBPF'+panelid})
        print('Found flex array: '+flexArray['code'])
        currentStage = flexArray['currentStage']['code']
        currentLocation = flexArray['currentLocation']['code']
        print("Current stage: " + currentStage)
        if currentStage == 'RCP':
            print('====== Next step: transition to SMD_LOAD stage...')
            pbv3_panel_transition.panel_transition(client, panelid, 'SMD_LOAD', force=True)
            flexArray = client.get('getComponent', json={'component':'20USBPF'+panelid})
            currentStage = flexArray['currentStage']['code']
            updateSheet(sheet, irow, flexArray, panelid, client, False)
        if currentStage == 'SMD_LOAD':
            print('====== Next step: load SMD components (linPOL, bPOL)...')
            pbv3_panel_smdload.loop_smdload_pb(panelid, client, bpol_type=bpol_type, linpol_type='LINPOL12V', currentLocation = currentLocation)
            print('====== Next step: transition to MAN_LOAD stage...')
            pbv3_panel_transition.panel_transition(client, panelid, 'MAN_LOAD', force=True)
            flexArray = client.get('getComponent', json={'component':'20USBPF'+panelid})
            currentStage = flexArray['currentStage']['code']
            updateSheet(sheet, irow, flexArray, panelid, client, False)
        if currentStage == 'MAN_LOAD':
            print('====== Next step: load coils...')
            pbv3_panel_loadcoils.soldercoils(client, panelid, currentLocation)
            print('====== Next step: load shield box...')

            sb_version = sheet.cell(irow, 2).value
            time.sleep(1)
            sb_batch = sheet.cell(irow, 3).value
            time.sleep(1)
            if (sb_version is not None) and (sb_batch is not None):
                for icol in range(4, 14):
                    pb_num = sheet.cell(irow, icol).value
                    time.sleep(1)
                    if pb_num is not None:
                        try:
                            pbv3_panel_loadshieldbox.loadshieldbox(client, panelid, icol-4, int(pb_num), sb_version = int(sb_version), sb_batch = int(sb_batch))
                        except Exception:
                            print('Seen error when load shield box on '+panelid+', pb '+str(pb_num))
                print('====== Next step: singulate flex array and bond powerboard to carrier card...')
                pbv3_panel_singulate.singulate(client, panelid, carrier_card_batch)
            flexArray = client.get('getComponent', json={'component':'20USBPF'+panelid})
            currentStage = flexArray['currentStage']['code']
            updateSheet(sheet, irow, flexArray, panelid, client, False)

        # now get panel again:
        try:
            panel = client.get('getComponent', json={'component':panelid, 'alternativeIdentifier':True})
            updateSheet(sheet, irow, panel, panelid, client, False)
        except Exception:
            print('Carrier card of ' + panelid +' not found in DB..')

    if panel is not None:
        print('Panel is on carrier card '+panel['code'])
        currentStage = panel['currentStage']['code']
        currentLocation = panel['currentLocation']['code']
        if currentStage == 'MAN_LOAD':
            print('====== Next step: transition to BONDED stage...')
            pbv3_panel_transition.panel_transition(client, panelid, 'BONDED', force=True)
            print('====== Next step: load hv mux...')
            pbv3_panel_loadhvmux.loadhvmux(client, panelid, currentLocation)
            print('====== Next step: load amac...')
            
            cell_wafer = sheet.find("AMAC wafer number")
            time.sleep(1)
            icol_wafer = cell_wafer.col
            wafer = sheet.cell(irow, icol_wafer).value 
            time.sleep(1)
            for icol in range(icol_wafer+1, icol_wafer+11):
                die_num_s = str(sheet.cell(irow, icol).value)
                time.sleep(1)
                die_num = 0
                amac_sn = ''
                if "20US" in die_num_s:
                    amac_sn = die_num_s
                elif die_num_s[0].isdigit():
                    die_num = int(die_num_s)
                else:
                    die_num = int(die_num_s[1:])
                try:
                    pbv3_panel_loadamac.loadwafer(client, panelid, icol-icol_wafer-1, wafer, die_num, amac_sn)
                except Exception:
                    print("Ooooops.... Failed to load amac "+str(die_num))
            
            panel = client.get('getComponent', json={'component':panelid, 'alternativeIdentifier':True})
            currentStage = panel['currentStage']['code']
        if currentStage == "LOADED" or currentStage == "HYBBURN": # PB is no longer on panel anymore
            updateSheet(sheet, irow, panel, panelid, client, checkTests=False)
        else:
            updateSheet(sheet, irow, panel, panelid, client, checkTests=checkTests, checkAMAC = checkAMAC)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Auto register powerboards from google sheet.")
    #parser.add_argument("-b","--bpol"     , default='BPOL12V4',     help="Assemble bPOL12V of given type.") # pre-production
    parser.add_argument("-b","--bpol"     , default='BPOL12V6',     help="Assemble bPOL12V of given type.") # production
    #parser.add_argument("-c","--carrier"  , default='20220112',     help="powerboard carrier card batch.") # pre-production B batch
    parser.add_argument("-c","--carrier"  , default='20230315',     help="powerboard carrier card batch.") # production batch
    #parser.add_argument("-f","--flex"     , default='2021-12-01',   help="powerboard flex array batch.") # pre-production B batch
    parser.add_argument("-f","--flex"     , default='20230315',   help="powerboard flex array batch.") # production batch
    parser.add_argument("-t","--waittime" , default=1,             help="Time (minutes) to wait to repeat the scan of the whole google sheet")
    #parser.add_argument("-k","--key"      , default='1GjACRhuTt9wX9Ld3mPJSpS3wtO5JtTXrY_0NnrtGnJQ',             help="Google sheet file key string") # pre-production
    parser.add_argument("-k","--key"      , default='1N4Kl-qvqpxf2iCZBP8q3kBTFORg6aUnc27eOJviNRT4',             help="Google sheet file key string") # production
    parser.add_argument("code1"           , help="Access Code 1.")
    parser.add_argument("code2"           , help="Access Code 2.")

    args = parser.parse_args()

    user = itkdb.core.User(args.code1, args.code2)
    user.authenticate()

    if user.is_authenticated():
        print('itkdb login successful!')
    else:
        print('Login unsuccessful...')
        sys.exit(1) 

    print('itkdb authenticate will expire in : '+str(user.expires_in) + ' s')

    client=itkdb.Client(user=user)

    gc = gspread.service_account(filename='googleToken.json')
   
    '''
    # block of code to test single panel 
    gsheet = gc.open_by_key(args.key)
    time.sleep(1)
    sheets = gsheet.worksheets()
    time.sleep(1)
    sheet=sheets[0]
    updatePanel(sheet, 96, client, checkTests=False, checkAMAC = True, flex_array_batch=args.flex, carrier_card_batch=args.carrier, bpol_type=args.bpol)
    '''

    while True:
        time_start = time.time()
        try:
            gsheet = gc.open_by_key(args.key)
            time.sleep(1)
            sheets = gsheet.worksheets()
            time.sleep(1)
            for isheet in range(len(sheets)):
                sheet = sheets[isheet]
                time.sleep(1)

                values_all = sheet.get_all_values()
                time.sleep(1)
                cell_ready = sheet.find("Ready for DB register?")
                time.sleep(1)
                if cell_ready is not None:
                    for irow in range(2, len(values_all)):
                        time.sleep(1)
                        if sheet.cell(irow+1, cell_ready.col).value == "Yes":
                            # check if login is expired
                            print('idkdb authenticate will expire in : '+str(user.expires_in) + ' s')
                            if user.is_expired() or user.expires_in < 300:
                                print('itkdb login: less than 5 mins left before expires => get new authentication now.')
                                user = itkdb.core.User(args.code1, args.code2)
                                user.authenticate()
                                client=itkdb.Client(user=user)
                            try:
                                updatePanel(sheet, irow+1, client, checkTests=True, checkAMAC=True, flex_array_batch=args.flex, carrier_card_batch=args.carrier, bpol_type=args.bpol)
                            except Exception:
                                print("Ooooops.... Failed to update panel "+str(sheet.cell(irow+1, 1).value))
                                time.sleep(1)
        except Exception:
            print("Something unexpected happened.... will continue... ")
        time_end = time.time()
        if time_end - time_start < 60*int(args.waittime):
            sleep_time = int(60*int(args.waittime) - time_end + time_start)
            print("Sleep for " + str(sleep_time) + " seconds before doing a new scan of the google sheet")
            time.sleep(sleep_time)
