#!/usr/bin/env python

import os, sys
import argparse
import itkdb

from database import pwbdbtools
from database import pwbdbpandas
from database import pwbdb


def mark_shield_box(client, sb_type, sb_version, sb_batch, pb_nums_str):

    pb_nums=set(pwbdbtools.parse_pwb_list(pb_nums_str))

    # 
    # Get existing shieldboxes and check for overlap
    pfilt=[
        #{'code': 'VERSION','operator': '=','value': str(sb_version)}, # does not work..
        {'code': 'BATCH'     ,'operator': '=','value': sb_batch}
        ]

    existing_shieldboxes=pwbdbpandas.listComponentsByProperty(client, 'P', componentType='PWB_SHIELDBOX', propertyFilter=pfilt)
    if len(existing_shieldboxes) > 0:
        existing_shieldboxes['VERSION'  ]=existing_shieldboxes.VERSION  .astype(int)
        existing_shieldboxes['PB_NUMBWE']=existing_shieldboxes.PB_NUMBWE.astype(int)

        existing_shieldboxes=existing_shieldboxes[existing_shieldboxes.VERSION==sb_version]
        existing_shieldboxes=existing_shieldboxes[existing_shieldboxes.type==sb_type]
        existing_shieldboxes=set(existing_shieldboxes.PB_NUMBWE.values)
        pb_nums_existing=pb_nums&existing_shieldboxes
        pb_nums         =pb_nums-existing_shieldboxes

        #
        # Print existing
        for pb_num in pb_nums_existing:
            print("Skip existing {:01d}{:02d} {:04d}".format(sb_version, sb_batch, pb_num))
    
    #
    # Start marking
    nget = len(pb_nums)+5
    if nget < 100:
        nget = nget*3
    shieldbox_codes=pwbdbtools.get_random_components(c=client, subproject='SB', comp_type='PWB_SHIELDBOX', sub_type=sb_type, n=nget, filt={'currentStage':'MANUFACTURED'})
    shieldboxes=map(lambda sb_code: pwbdb.Component(sb_code, client=client), shieldbox_codes)
    
    nused = 0
    for pb_num in pb_nums:
        print("Marking {:01d}{:02d} {:04d}".format(sb_version, sb_batch, pb_num))
        if nused >= len(pb_nums)*3:
            return
        sb=next(shieldboxes)
        nused += 1
        currentStage = sb.dump['currentStage']['code']
        while currentStage == 'MARKED':
            if nused >= len(pb_nums)*3:
                return
            sb=next(shieldboxes)
            nused += 1
            currentStage = sb.dump['currentStage']['code']
        sb.property('VERSION'  , str(sb_version))
        sb.property('BATCH'    , sb_batch)
        sb.property('PB_NUMBWE', pb_num)
        sb.currentStage='MARKED'


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Mark shield boxes with ID.")

    parser.add_argument("type"              , help="Shield Box type code.")
    parser.add_argument("version" , type=int, help="PWB version number.")
    parser.add_argument("batch"   , type=int, help="PWB batch number.")
    parser.add_argument("pwb_num"           , help="Comma-separated PWB numbers. Ranges are allowed (ie: 1,3-5).")

    args = parser.parse_args()

    c = pwbdbtools.get_db_client()

    mark_shield_box(c, args.type, args.version, args.batch, args.pwb_num)
