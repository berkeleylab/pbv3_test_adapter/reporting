#!/usr/bin/env python

import os, sys
import argparse
import math

import itk_pdb.dbAccess as dbAccess

import json
import pandas as pd
import jinja2
import matplotlib.pyplot as plt

def load_bulk_component(codes):
    """
    Fetch information for several powerboards.

    Parameters:
    codes (list): list of codes corresponding to powerboard components

    Returns:
    DataFrame: information of requested powerboards
    """
    df_powerboards=[]
    componentList = dbAccess.doSomething('getComponentBulk', {'component': codes}, method='GET')['itemList']
    for component in componentList:
        # Remove unneeded columns that are complicated
        component.pop('attachments',None)
        component.pop('comments'   ,None)
        component.pop('parents'    ,None)
        component.pop('children'   ,None)
        component.pop('tests'      ,None)
        component.pop('stages'     ,None)
        component.pop('grades'     ,None)

        # Make properties useful
        for property in component['properties']:
            component[property['code']]=property.get('value',property['default'])
        component.pop('properties'  ,None)

        # Flatten remaining attributes
        for key in list(component.keys()):
            if type(component[key])!=dict: continue
            for k,v in component[key].items():
                component[key+'_'+k]=v
            component.pop(key)

        df_powerboards.append(pd.DataFrame([component]))

    return pd.concat(df_powerboards)


def load_testType(pb_code,testType):
    testRuns=dbAccess.doSomething('listTestRunsByComponent',{'component':pb_code,'testType':testType},method='GET')['pageItemList']
    testRunIDs=[testRun['id'] for testRun in testRuns if testRun['state']!='deleted']

    return load_testRuns(testRunIDs)

def load_testRuns(ids):
    df_result=pd.DataFrame({'runNumber':[]})
    
    testRunList = dbAccess.doSomething('getTestRunBulk', {'testRun':ids}, method='GET')['testRunList']
    for testRun in testRunList:
        # Remove unneeded columns
        testRun.pop('defects', None)
        testRun.pop('comments', None)
        testRun.pop('attachments', None)

        # Make properties and results useful
        hasList=False
        for property in testRun['properties']:
            testRun[property['code']]=property['value']
            testRun.pop('properties', None)
        for result in testRun['results']:
            if result['valueType']=='array': hasList=True
            testRun[result['code']]=result['value']
            testRun.pop('results', None)
            
        # Flatten columns by taking only id/code
        for key in list(testRun.keys()):
            if type(testRun[key])!=dict: continue
            if 'id' in testRun[key]:
                testRun[key+'_id']=testRun[key]['id']
            if 'code' in testRun[key]:
                testRun[key+'_code']=testRun[key]['code']
            testRun.pop(key)

        df=pd.DataFrame(testRun if hasList else [testRun])
        df_result=df_result.append(df,sort=False)

    return df_result

def generate_report(pb_serialNumber,pb_code,template,outdir='output/'):
    #
    # Make directory for plots
    plotdir='{}/{}'.format(outdir,pb_serialNumber)
    if not os.path.exists(plotdir):
        os.mkdir(plotdir)
    
    #
    # Efficiency plot    
    df_DCDCEFFICIENCY=load_testType(pb_code,'DCDCEFFICIENCY')
    if not df_DCDCEFFICIENCY.empty:
        df_DCDCEFFICIENCY['myeff']=(1.5*df_DCDCEFFICIENCY['IOUT'])/(11*(df_DCDCEFFICIENCY['IIN']-df_DCDCEFFICIENCY['IINOFFSET']))

        for rn,rndata in df_DCDCEFFICIENCY.groupby('runNumber'):
            plt.plot(rndata['IOUT'],rndata['myeff'],'o')
    plt.xlim(0,3.5)
    plt.ylim(0,1.)
    plt.xlabel('Output Current [A]')
    plt.ylabel('DC/DC Efficiency')
    plt.savefig('{}/dcdcefficiency.png'.format(plotdir))
    plt.clf()

    #
    # Calibration curve
    df_AMSLOPE=load_testType(pb_code,'AMSLOPE')
    if not df_AMSLOPE.empty:
        for rn,rndata in df_AMSLOPE.groupby('runNumber'):
            plt.plot(rndata['CALIN'],rndata['AMACCAL'],'o')
    plt.xlim(0,1)
    plt.ylim(0,1024)
    plt.xlabel('External CAL [V]')
    plt.xlabel('AMAC CAL [counts]')
    plt.savefig('{}/amslope.png'.format(plotdir))
    plt.clf()    

    #
    # Input current
    if not df_DCDCEFFICIENCY.empty:
        for rn,rndata in df_DCDCEFFICIENCY.groupby('runNumber'):
            plt.plot(rndata['IIN'],rndata['AMACCUR10V'],'.k')

    plt.xlim(0,1)
    plt.ylim(0,1024)            
    plt.xlabel('Input Current [A]')
    plt.ylabel('AMAC Input Current [counts]')
    plt.savefig('{}/curin.png'.format(plotdir))
    plt.clf()    

    #
    # Output current
    if not df_DCDCEFFICIENCY.empty:
        for rn,rndata in df_DCDCEFFICIENCY.groupby('runNumber'):
            plt.plot(rndata['IOUT'],rndata['AMACCUR1V'],'.k')

    plt.xlim(0,3)
    plt.ylim(0,1024)            
    plt.xlabel('Output Current [A]')
    plt.ylabel('AMAC Output Current [counts]')
    plt.savefig('{}/curout.png'.format(plotdir))
    plt.clf()    

    #
    # HV Sense curve
    testRuns=dbAccess.doSomething('listTestRunsByComponent',{'component':pb_code,'testType':'HVSENSE'},method='GET')['pageItemList']
    testRunIDs=[testRun['id'] for testRun in testRuns if testRun['state']!='deleted']

    df_HVSENSE=load_testRuns(testRunIDs)
    if not df_HVSENSE.empty:
        plt.semilogx(df_HVSENSE['HVI'],df_HVSENSE['AMACGAIN0'],'.',label='0')
        plt.semilogx(df_HVSENSE['HVI'],df_HVSENSE['AMACGAIN1'],'.',label='1')
        plt.semilogx(df_HVSENSE['HVI'],df_HVSENSE['AMACGAIN2'],'.',label='2')
        plt.semilogx(df_HVSENSE['HVI'],df_HVSENSE['AMACGAIN4'],'.',label='4')
        plt.semilogx(df_HVSENSE['HVI'],df_HVSENSE['AMACGAIN8'],'.',label='8')

    plt.xlabel('HV Current [A]')
    plt.ylabel('AMAC Reading [counts]')
    plt.legend(title='Gain',frameon=False)
    plt.xlim(1e-9,1e-3)
    #plt.xticks([1e-9,1e-8,1e-7,1e-6,1e-5,1e-4,1e-3])
    plt.savefig('{}/hvsense.png'.format(plotdir))

    #
    # Write output
    output=template.render(serialNumber=pb_serialNumber)    
    fh=open('{}/{}.html'.format(outdir,pb_serialNumber),'w')
    fh.write(output)
    fh.close()

#
# Main program!
#   
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Create HTML reports of Powerboard test data from the production database.")
    parser.add_argument("output", help="Path to output folder for the HTML code.")
    parser.add_argument("pass1" , help="First ITk database password.")
    parser.add_argument("pass2" , help="Second ITk database password.")    
    parser.add_argument("--verbose", action="store_true",
                            help="Print what's being sent and received")    

    args = parser.parse_args()

    #
    # Initialize database session
    if args.verbose:
        dbAccess.verbose = True

    if os.getenv("ITK_DB_AUTH"):
        dbAccess.token = os.getenv("ITK_DB_AUTH")
    else:
        os.environ["ITK_DB_AUTH"] = dbAccess.authenticate(args.pass1, args.pass2)

    #
    # Prepare the jinja template
    file_loader = jinja2.FileSystemLoader('template')
    env = jinja2.Environment(loader=file_loader)

    #
    # Get list of all Powerboards
    componentList = dbAccess.doSomething("listComponents", {'project':'S', 'subproject':'SB', 'institution':'LBL', 'componentType':'PWB'}, method='GET')['pageItemList']
    powerboard_ids=[]
    for component in componentList:
        powerboard_ids.append(component['code'])
    print('Found {} registered Powerboards'.format(len(powerboard_ids)))

    # Split into chunks of maximum 50 components for pagination
    nchunks=math.ceil(len(powerboard_ids)/50)
    chunks_powerboard_ids=[powerboard_ids[i*50:min((i+1)*50,len(powerboard_ids))] for i in range(nchunks)]

    # Build DataFrame of Powerboards, one chunk at a time
    print('Query Powerboard information')
    df_powerboards=pd.concat(map(load_bulk_component,chunks_powerboard_ids))

    t=env.get_template('index.html')
    output=t.render(powerboards=df_powerboards)

    fh=open(args.output+'/index.html','w')
    fh.write(output)
    fh.close()

    #
    # Generate report pages
    for pb_serialNumber, pb_code in zip(df_powerboards.serialNumber.values, df_powerboards.code.values):
        print("Processing {}".format(pb_serialNumber))
        generate_report(pb_serialNumber,pb_code,template=env.get_template('report.html'), outdir=args.output)
    
