# Powerboard Intentory Script

Determines the total number of various powerboard components and powerboards.

## Usage

```sh
./inventory.py output_path first-itk-db-password second-itk-db-password
./report.py output_path/reports first-itk-db-password second-itk-db-password
```

The `output_path` should be a folder accessible via a web server (ie: `/home/${USER}/public_html`).
