# Setup config.json file

The configuration of the application is stored inside the `webreport/pwbDashSummaryPlots/config.json` file. It is a dictionary with the following keys:

* `saveLocation`: Absolute path to the cache of the data.
* `requests_pathname_prefix`: Any path past the hostname in the URL. See [Integrating Dash with Existing Web Apps.](dash.plotly.com/integrating-dash)

# Caching test data from ITk Production Database

In order for the Dash application to display any test results, you must pull the data from the ITk Production Database. In order to do so, run `production_database_scripts/strips/pwbs/getTestData.py` with the following three arguments:
   1. Password 1 to the ITk Production Database
   2. Password 2 to the ITk Production Database
   3. Location in which to store the data (in the form of json files). This should be the same as `saveLocation` as given in the `config.json` file.

It is advised that you run this script routinely, either manually, or by setting up a cronjob, so that the test results are up to date.

# Setup for Apache

Follow the installation instructions for the reporting package. The rest of this section will assume that the virtual environment is setup at `/var/www/reporting/venv`.

If you already have a website up and running that you would like to host the Dash application at, then you will need to add the following to the `.conf` file containing your VirtualHost:

```
    WSGIDaemonProcess  python-home=/var/www/reporting/venv user=powerboard
    WSGIProcessGroup summaryplots
    WSGIScriptAlias /summaryplots /var/www/reporting/webreport/pwbDashSummaryPlots/wsgi.py process-group=testsummaryplots
```

This uses the virtual environment located at `/var/www/reporting/venv` using the mod\_wsgi definition from `/var/www/reporting/webreport/pwbDashSummaryPlots/wsgi.py`. It is run as the `powerboard` user. The website will then be accessible at hostname.com/summaryplots. If you are using an alias other than `/`, make sure to edit `config.json`.

If you are not yet hosting a website on the server, you will need to define a VirtualHost. Create a file titled pwbDashPlotswsgi.conf with the following contents at /etc/httpd/conf.d:

```
    <VirtualHost *:80>

        ServerName {$SERVER_NAME}

        WSGIDaemonProcess summaryplots python-home=/var/www/pwbDashPlots

        WSGIScriptAlias /summaryplots /var/www/pwbDashPlots/wsgi.py

        <Directory /var/www/pwbDashPlots>
            Require all granted
        </Directory>

    </VirtualHost>
```

Note that in order for this to begin running, you must restart Apache. This is done by running: `sudo systemctl restart httpd`

Also note that the file location of `pwbDashPlotswsgi.conf` might differ if you are not on CentOS, as Apache is called apache, not httpd. Restarting Apache will also differ.

Make sure that you have read and write permissions for the testResult json files.

# Setup for Local Running

If you wish to run this locally, in the event that you are developing the application or do not have a server to run the application on, do the following:

Run `run.py` from the `reporting` directory and the application should be viewable only on the machine you are running by visiting 127.0.0.1:8050 (check that the port is 8050 by checking output from running `run.py`. If you get an error that the port is in use, change `app.run_server(debug=True)` to `app.run_server(debug=True, port=PORT)` where `PORT` is the port number you wish to run the application on.

