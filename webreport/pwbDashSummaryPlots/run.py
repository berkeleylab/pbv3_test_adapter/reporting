#!/usr/bin/env python3
import os, sys
from app import app

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.server.run(host="0.0.0.0", port=port, debug=True)
