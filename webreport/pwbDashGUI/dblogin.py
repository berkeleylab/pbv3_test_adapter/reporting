import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate

import codecs
import pickle
import datetime
import pkg_resources

from webreport.pwbDashCommon import template

import itkdb.core

class DBLoginPage:
    def __init__(self, app):
        # Create the layout
        self.layout = template.jinja2_to_dash(
            pkg_resources.resource_filename(__name__,'template'),
            'dblogin.html')

        #
        # Callbacks
        app.callback(
            [Output('itkdb_auth', 'data'),Output('itk_message', 'children'),
             Output('itkdb_auth_pass1', 'data'), 
             Output('itkdb_auth_pass2', 'data')], 
            [Input('itk_button','n_clicks')],
            [
                State('itk_pass1', 'value'),
                State('itk_pass2','value')
            ],
            prevent_initial_call=True)(self.login)

        app.callback(
            [Output('itkdb_status', 'children')],
            [Input('itkdb_auth','data')],
            prevent_initial_call=True)(self.status)
        
    def login(self, button, pass1, pass2):
        if button==None:
            raise PreventUpdate

        user = itkdb.core.User(pass1,pass2)
        user.authenticate()

        if user.is_authenticated():
            return codecs.encode(pickle.dumps(user),'base64').decode(), "Login successful!", pass1, pass2
        else:
            return user, "Login unsuccessful...", pass1, pass2

    def status(self, user):
        if user==None or user=='':
            return 'Login',

        user = pickle.loads(codecs.decode(user.encode(),'base64'))
        
        return user.name,
        
