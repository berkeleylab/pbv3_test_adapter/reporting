import time
import re

class BlockParser:
    """
    Block parse looks for start and end patterns that
    indicate a block. When inside block, it sets the 
    `inblock` member variable to True. The `block_start`
    and `block_end` functions are also called when found.
    """
    def __init__(self, start, end):
        self.start=re.compile(start)
        self.end=re.compile(end)

        self.inblock=False
        self.lastUpdate=time.time()

    def touch(self):
        self.lastUpdate=time.time()

    def clear(self):
        """ Reset the state of any internal data """
        self.inblock=False
        self.touch()
        
    def parse(self,line):
        if self.start.search(line)!=None:
            self.inblock=True
            self.block_start()
            return True

        if self.end.search(line)!=None:
            self.block_end()
            self.inblock=False
            return True

        return False
 
    def block_start(self):
        """ Called when start of block detected """
        pass

    def block_end(self):
        """ Called when start of block detected """
        pass
    
class StreamParser:
    def __init__(self):
        self.parsers=[]

    def parse(self, line):
        #
        # Parse the line
        for parser in self.parsers:
            parser.parse(line)
