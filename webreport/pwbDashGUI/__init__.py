import dash
import dash_auth
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

import os
import pkg_resources

from webreport.pwbDashCommon import template
from . import dblogin
from . import testing
from . import display
from . import thermal
from . import config

def create_app(cfg_path, page_title="Almighty Powerboard Tester", port=5000):
    """ Create Dash app for the Powerboard GUI """

    #
    # App instance
    external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
    app = dash.Dash(__name__, title=page_title, external_stylesheets=external_stylesheets, update_title=None, suppress_callback_exceptions=True)

    try:
        #
        # Configuration
        cfg=config.Config(cfg_path)

        # Authentication
        auth = dash_auth.BasicAuth(
            app, {}
        )
        cfg.auth=auth

        # Layout
        location=dcc.Location(id='url', refresh=False)

        app.layout = template.jinja2_to_dash(pkg_resources.resource_filename(__name__,'template'),
                                             'index.html',replace=[location])

        #
        # Routes
        page_testing = testing.TestingPage(cfg, app, port)
        page_display = display.DisplayPage(cfg, app)
        page_thermal = thermal.ThermalPage(cfg, app, port)
        page_dblogin = dblogin.DBLoginPage(app)
        page_config  = config.ConfigPage(cfg, app)

        @app.callback(
            Output('page-content', 'children'),
            [Input('url', 'pathname')]
        )
        def display_page(pathname):
            if pathname=='/itkdb':
                return page_dblogin.layout
            elif pathname=='/config':
                return page_config .layout
            elif pathname=='/thermal':
                return page_thermal .layout
            elif pathname=='/display':
                return page_display .layout
            return page_testing.layout

    except Exception as err:
        print(err)
        print(type(err))
        app.layout=html.Div(err.args)

    return app
