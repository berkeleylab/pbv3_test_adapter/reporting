import subprocess
import threading
from queue import Queue

class ProcessMonitor(threading.Thread):
    """
    ProcessMonitor is an asynchronous monitor of an
    external process.

    The thread starts a process given by `cmd` and monitor
    the output. The output is saved into `obj.queue` that 
    can then be accessed from other threads.
    """
    def __init__(self, cmd, cwd=None):
        super(ProcessMonitor, self).__init__()
        self.queue = Queue()
        self.cmd = cmd
        self.cwd = cwd

        self.proc = None

    def terminate(self):
        self.proc.terminate()
        
    def run(self):
        self.proc = subprocess.Popen(self.cmd, cwd=self.cwd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        for line in self.proc.stdout:
            self.queue.put(line.decode().strip())
