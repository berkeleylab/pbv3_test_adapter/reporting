import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_uploader
from dash.dependencies import Input, Output, State

import os
import json
import shutil
import tempfile
import hashlib
import pkg_resources

from webreport.pwbDashCommon import template

class Config:
    """
    Configuration of the Powerboard testing GUI.

    The configuration is stored in a json file with the following
    keys.
    - `version` : Version of the config file. Must match the version from this class.
    - `username`: Username for http login
    - `password`: Password for http login
    - `institution`: institution of where the test is performed
    - `fwdir`: Location of firmware.
    - `panels`: List of connected active boards. See Panel Configuration.
    - `chillers`: List of connected chillers.
    - `autoQC_cfg`: config for auto QC at LBNL.

    ## Panel Configuration

    Each panel is specified as a dictionary with the following keys:
    - `name`: Name used to refer to the panel.
    - `runner`: Name of class in the `panel` module used to run the panel.

    All extra keys are passed to the contructor of the `runner` class as kwargs.
    """
    def __init__(self, confpath):
        self.confpath=confpath

        self.__auth=None

        # Configs
        self.version   =0.1
        self.runAutoQC=False
        self.__username='admin'
        self.__password='kkisawesome'
        self.__institution=''
        self.fwdir     =None
        self.panels    =[]
        self.chillers  =[]
        self.autoQC_cfg  =None

        if os.path.exists(confpath):
            self.load()

    def load(self):
        with open(self.confpath) as json_file:
            data = json.load(json_file)
            if data.get('version',None)!=self.version:
                raise Exception('Wrong config file version (excepted: {}, got: {})!'.format(self.version, data.get('version','unknown')))
            self.runAutoQC  =data.get('runAutoQC',self.runAutoQC)
            self.username  =data.get('username',self.username)
            self.password  =data.get('password',self.password)
            self.institution  =data.get('institution',self.institution)
            self.fwdir     =data.get('fwdir'   ,self.fwdir   )
            self.panels    =data.get('panels',[{'name':f'{i}', 'ip':'', 'config':'equip_testbench.json'} for i in range(10)])
            self.chillers    =data.get('chillers',[{'name':f'{i}'} for i in range(10)])
            self.autoQC_cfg     =data.get('autoQC_cfg'   ,self.autoQC_cfg   )

    def save(self):
        with open(self.confpath,'w') as json_file:
            json.dump({
                'version'    : self.version,
                'runAutoQC'   : self.runAutoQC,
                'username'   : self.username,
                'password'   : self.password,
                'institution': self.institution,
                'fwdir'      : self.fwdir,
                'panels'     : self.panels,
                'chillers'   : self.chillers,
                'autoQC_cfg'   : self.autoQC_cfg
                }, json_file, indent=4)

    #
    # Tools
    @property
    def auth(self):
        return self.__auth

    @auth.setter
    def auth(self, newauth):
        self.__auth=newauth
        self.update_auth()

    def update_auth(self):
        if self.__auth==None:
            return
        self.__auth._users={self.username: self.password}

    #
    # Configurations
    @property
    def username(self):
        return self.__username

    @username.setter
    def username(self, username):
        self.__username=username
        self.update_auth()

    @property
    def password(self):
        return self.__password

    @password.setter
    def password(self, password):
        self.__password=password
        self.update_auth()

    @property
    def institution(self):
        return self.__institution
    
    @institution.setter
    def institution(self, institution):
        self.__institution=institution
    
class ConfigPage:
    def __init__(self, cfg, app):
        self.cfg=cfg

        dash_uploader.configure_upload(app, tempfile.mkdtemp())

        # Password
        cfg_pass1=dcc.Input(id='cfg_pass1',type='password')
        cfg_pass2=dcc.Input(id='cfg_pass2',type='password')
        app.callback(
            [
                Output('cfg_chpass_message', 'children'),
                Output('cfg_passbtn', 'disabled'),
            ],[
                Input('cfg_pass1','value'),
                Input('cfg_pass2','value')
            ])(self.check_password)

        app.callback(
            Output('div_passbtnplaceholder', 'children'),
            [
                Input('cfg_passbtn','n_clicks')
            ],[
                State('cfg_pass1','value')
            ],prevent_initial_call=True)(self.save_password)

        # Institution
        cfg_institution=dcc.Input(id='cfg_institution', value="", placeholder="institution_code")
        app.callback(
            Output('cfg_chinstitution_message', 'children'),
            [
                Input('cfg_institutionbtn','n_clicks')
            ],[
                State('cfg_institution','value')
            ],prevent_initial_call=True)(self.set_institution)


        # Firmware upload
        fwupload = dash_uploader.Upload(id='cfg_firmware',
                                        text="""
	                                Drag and drop or click to select a file to upload.
                                        """,
                                        filetypes=['ub','bin','scr']
        )
        dash_uploader.callback(
            Output('cfg_fw_message', 'children'),
            id='cfg_firmware'
        )(self.upload_firmware)

        # System control
        app.callback(
            Output('cfg_ctl_message', 'children'),
            [
                Input('cfg_rebootbtn','n_clicks'),
                Input('cfg_shutdownbtn','n_clicks'),
            ],prevent_initial_call=True)(self.shutdown)


        # Create the layout
        self.layout = template.jinja2_to_dash(
            pkg_resources.resource_filename(__name__,'template'),
            'config.html',replace=[cfg_pass1, cfg_pass2, cfg_institution, fwupload])
        
    def check_password(self,pass1,pass2):
        """
        Check if new password is valid
        """

        if pass1!=pass2:
            return 'Password confirmation does not match.',True
        return '',False

    def save_password(self,n_clicks,pass1):
        """
        Save password to file
        """
        self.cfg.password=pass1
        self.cfg.save()
        return ''

    def set_institution(self,n_clicks,institution):
        """
        Save institution to file
        """
        if institution == "":
            return 'No institution provided!'
        self.cfg.institution=institution
        self.cfg.save()
        return 'Institution set! Please note that no validity check is performed and make sure your institution code is correct.'

    def upload_firmware(self, filenames):
        """
        Upload firmware files
        """

        retdiv=html.Div(children=[])
        for path in filenames:
            filename=os.path.basename(path)
            if filename not in ['image.ub','BOOT.bin','boot.scr']:
                retdiv.children.append(html.Div("Unknown file: {}".format(filename),className="bad"))
                continue

            try:
                shutil.copyfile(
                    path,
                    '{}/{}'.format(self.cfg.fwdir,filename)
                )
                retdiv.children.append(html.Div("Success: {}".format(filename),className="good"))
            except FileNotFoundError:
                retdiv.children.append(html.Div("Error: {}".format(filename),className="bad"))
        return retdiv

    def shutdown(self,reboot,shutdown):
        """
        Shutdown/reboot the computer
        """
        ctx = dash.callback_context

        if not ctx.triggered:
            return ''

        button_id = ctx.triggered[0]['prop_id'].split('.')[0]
        if button_id=='cfg_rebootbtn':
            os.system('shutdown -r now')
            return 'Reboot request received. Please manually reload page in a few minutes.'

        if button_id=='cfg_shutdownbtn':
            os.system('shutdown -h now')
            return 'Shutdown requested. Please turn off test system in a few minutes.'
