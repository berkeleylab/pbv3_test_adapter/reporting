import re
import pandas as pd

from . import streamparser

class PBv3Parser(streamparser.BlockParser):
    """
    Base parser for all powerboard blocks. In addition to defining
    a block of data, it also looks for a line that indicates which
    powerboard is currently being tested.
    """
    def __init__(self,start,end):
        super().__init__(start,end)

        self.ppattern= re.compile('### Performing .* tests on powerboard ([0-9])+ ###')

        self.pb=None

    def clear(self):
        self.pb=None
        super().clear()

    def parse(self,line):
        # Check for start of a new powerboard
        match=self.ppattern.search(line)
        if match!=None:
            self.pb=int(match.group(1))
            return True

        # Block detection
        if super().parse(line): # this is start of a block
            return True        

        return False

class DataParser(PBv3Parser):
    def __init__(self,start,end,columns=[]):
        super().__init__(start,end)

        self.data = pd.DataFrame(columns=['pb']+columns)
        self.header=None

    def clear(self):
        super().clear()
        self.data = self.data.iloc[0:0]

    def parse(self,line):
        # Check for start of a new powerboard and blocks
        if super().parse(line):
            self.header=None
            return True

        if not self.inblock: # Not in a block...
            return False

        # Try to parse data

        # Skip info/error/debug lines
        if line.startswith('['):
            return False
        
        # Look for header
        parts=line.split()
        if self.header==None:
            self.header=parts
            return True

        # Data
        data=map(float,parts)
        point=dict(zip(self.header,data))
        point['pb']=self.pb

        self.data=self.data.append(point, ignore_index=True)
        self.touch()

        return True    

class SingleValueParser(PBv3Parser):
    """
    Parse checks of single value variables in a test.

    The input format is expected to be as follows. There can
    be multiple keys per text. The arrow indent (`-->`) is
    configurable.

    ```
    ## Test {testtype}
    [STATUS] --> key0: value0
    [STATUS] --> key1: value1
    ## End test {testtype}

    The output is a DataFrame with the following columns.
    - `value_{key0}` = `value0`
    - `error_{key0}` = True if `STATUS` == `ERROR`, False otherwise.
    ```
    """
    def __init__(self,testtype,prefix='-->',columns=[]):
        """
        Parameters:
         testtype (str): Test code indicating start and end of test output
         prefix (str): second column string indicating data
         columns (list of str): List of default column names
        """
        super().__init__(f"## Test {testtype}",f"## End test {testtype}")

        self.prefix=prefix

        self.data = pd.DataFrame(columns=['pb']+columns)
        self.data['pb'] = self.data.pb.astype(int)
        self.point = None

    def clear(self):
        super().clear()
        self.data = self.data.iloc[0:0]
        self.point = None

    def parse(self,line):
        # Check for start of a new powerboard and blocks
        if super().parse(line):
            return True

        if not self.inblock: # Not in a block...
            return False

        #
        # Try to parse data

        parts=line.split()
        if len(parts)<5: # Not enough columns
            return False

        # Changing status
        if parts[2]==self.prefix:
            key=parts[3][:-1] # Remove the last :
            value=float(parts[4])
            error= parts[0]=="[ERROR]"
            self.point['value_'+key] = value
            self.point['error_'+key] = error
            return True

    def block_start(self):
        # Reset state
        self.point = {}

    def block_end(self):
        # Store last point recorded
        self.store_point()
        self.point = None
        
    def store_point(self):
        if self.point==None:
            return # No data recorded
        # Data
        self.point['pb']=self.pb

        self.data=self.data.append(self.point, ignore_index=True)
        self.touch()

class TableParser(PBv3Parser):
    """
    Parse scans that are output as a table.

    The input format is expected to be as follows.

    ```
    ## Test {testtype}
    Col0Name   Col1Name   Col2Name
    Col0Value0 Col1Value0 Col2Value0
    Col0Value1 Col1Value1 Col2Value1
    ...
    ## End test {testtype}

    The output is a DataFrame with colum names from the first row and values
    for the corresponding ones.
    ```
    """
    def __init__(self,testtype,columns=[]):
        """
        Parameters:
         testtype (str): Test code indicating start and end of test output
         columns (list of str): List of default column names
        """
        super().__init__(f"## Test {testtype}",f"## End test {testtype}")

        self.data = pd.DataFrame(columns=['pb']+columns)

        self.data['pb'] = self.data.pb.astype(int)
        self.columns = None

    def clear(self):
        self.data = self.data.iloc[0:0]
        super().clear()

    def parse(self,line):
        # Check for start of a new powerboard and blocks
        if super().parse(line):
            return True

        if not self.inblock: # Not in a block...
            return False

        #
        # Try to parse data
        parts=line.split()

        # First line is header
        if self.columns is None:
            self.columns=parts
            return True

        if len(parts)!=len(self.columns): # Wrong number of columns
            # TODO: print error?
            return False

        # Record data
        parts=[float(p) for p in parts]
        point=dict(zip(self.columns, parts))
        point['pb']=self.pb
        self.data=self.data.append(point, ignore_index=True)
        self.touch()

        return True

    def block_start(self):
        # Reset state
        super().block_start()
        self.columns = None

class VEnParser(PBv3Parser):
    def __init__(self,testtype,columns=[]):
        super().__init__(f"## Test {testtype}",f"## End test {testtype}")

        self.data = pd.DataFrame(columns=['pb']+columns)
        self.point_common = {}
        self.point = None

    def clear(self):
        super().clear()
        self.data = self.data.iloc[0:0]
        self.point_common = {}
        self.point = None

    def parse(self,line):
        # Check for start of a new powerboard and blocks
        if super().parse(line):
            return True

        if not self.inblock: # Not in a block...
            return False

        #
        # Try to parse data

        parts=line.split()

        # Changing status
        if parts[2]=='-->':
            # Store previous point if needed
            self.store_point()

            # Create new point
            self.point = self.point_common.copy()
            if parts[3]=='Enabling':
                self.point[parts[4]] = 1
            if parts[3]=='Disabling':
                self.point[parts[4]] = 0
            return True

        # Data point
        if parts[2]=='--->':
            key=parts[3][:-1]
            value=float(parts[4])
            error= parts[0]=="[ERROR]"
            if self.point!=None: # Started gathering data
                self.point["value_"+key]=value
                self.point["error_"+key]=error
            else: # Common part
                self.point_common[key]=value
            return True

    def block_start(self):
        # Reset state
        self.point = None
        self.point_common = {}

    def block_end(self):
        # Store last point recorded
        self.store_point()
        
    def store_point(self):
        if self.point==None:
            return # No data recorded
        # Data
        self.point['pb']=self.pb

        self.data=self.data.append(self.point, ignore_index=True)
        self.touch()

class OFParser(VEnParser):
    def __init__(self):
        super().__init__("OF",[])

class LVEnParser(VEnParser):
    def __init__(self):
        super().__init__("LV_ENABLE",[])

class HVEnParser(VEnParser):
    def __init__(self):
        super().__init__("HV_ENABLE",[])

class ToggleOutputParser(PBv3Parser):
    def __init__(self):
        super().__init__("## Test TOGGLEOUTPUT","## End test TOGGLEOUTPUT")

        self.pattern = re.compile("(INFO|ERROR)]\s+:\s+(.*):\s+(-?\d*.\d*|-?\d*.\d*e-?\d*)\s+(-?\d*.\d*|-?\d*.\d*e-?\d*)")

        self.data = pd.DataFrame(columns=['pb'])

    def clear(self):
        super().clear()
        self.data=self.data.iloc[0:0]
        self.touch()

    def parse(self,line):
        # Check for start of a new powerboard and blocks
        if super().parse(line):
            return True

        if not self.inblock: # Not in a block...
            return False

        # Try to parse data
        matches = self.pattern.search(line)
        if matches==None:
            return False

        tio_ie = matches.group(1)
        tio_reg = matches.group(2)
        tio_off = matches.group(3)
        tio_on = matches.group(4)

        self.data=self.data.append({
            'pb'    : self.pb,
            'output': tio_reg,
            'state' : True,
            'value' : float(tio_on),
            'error' : tio_ie!="INFO"},
                                       ignore_index=True)

        self.data=self.data.append({
            'pb'     : self.pb,
            'output' : tio_reg,
            'state'  : False,
            'value'  : float(tio_off),
            'error'  : tio_ie!="INFO"},
                                       ignore_index=True)
        self.touch()

        return True

class LVIVParser(DataParser):
    def __init__(self):
        super().__init__("--> Running from 6V to 11V[^\\n]*\s([^\[?]*)","##",['VIN','IIN','AMACDCDCIN'])

class DCDCEffParser(DataParser):
    def __init__(self):
        super().__init__("## Measuring DCDC efficiency","##",['IOUTSET','IOUT','EFFICIENCY'])

class AMSlopeParser(DataParser):
    def __init__(self):
        super().__init__("## Calibrating AMAC slope","##",['CALIN','AMbg','AMintCalib','AMACCAL'])

class HVScanParser(DataParser):
    def __init__(self):
        super().__init__("## Measure HV sense","##",['HVIOUT','AMACGAIN0','AMACGAIN1','AMACGAIN2','AMACGAIN4','AMACGAIN8'])
        
class DACParser(TableParser):
    def __init__(self, testtype):
        super().__init__(testtype,['DAC','External','Internal'])
