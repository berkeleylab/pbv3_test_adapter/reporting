import os
import re
import threading

from . import procmon
from . import streamparser
from . import pbv3parser

class ICommandRunner:
    """
    Controls running and parsing the outputs of chiller control.

    This is an interface class. It should not be used directly.

    The running of a chiller control is done by executing a command and parsing the
    output. The command is build by the `command` function that subclasses
    classes have to implement.
    
    Thread safe

    """
    def __init__(self):
        # Process data
        self.proc=None
        self.data=[]
        self.lock=threading.Lock()

        # Parsing helpers
        self.reaesc = re.compile(r'\x1b[^m]*m')        
        self.sparser = streamparser.StreamParser()

    @property
    def running(self):
        if self.proc==None:
            return False
        return self.proc.is_alive()

    def stop(self):
        """ Terminate the current running program """
        if not self.running:
            return

        self.proc.terminate()
        self.proc.join()

    def run(self, program='chiller_cycle', option=None, useConfig=True, save_influx=False, save_csv=False, debug=0):
        self.stop()

        self.lock.acquire(blocking=True) # Make sure we are the only runners
        #
        # Start the run command
        self.data=[]
        for parser in self.sparser.parsers:
            parser.clear()
        cmd=self.command(program=program, option=option, useConfig=useConfig, save_influx=save_influx, save_csv=save_csv, debug=debug)
        self.proc = procmon.ProcessMonitor(cmd)
        self.proc.start()
        self.lock.release()

    def parse(self):
        if self.proc==None: return
            
        if not self.lock.acquire(blocking=False):
            return # Someone else will parse...

        while self.proc.queue.qsize()>0:
            line=self.proc.queue.get(block=False)
            print(line)
            line=self.reaesc.sub('',line)
            self.data.append(line)
            self.sparser.parse(line)

        self.lock.release()

    def command(self, program='chiller_cycle', option=None, useConfig=True, save_influx=True, save_csv=True, debug=0):
        """
        Command to execute. All inheriting classes need to implement this.

        The return value should be the command formatted in a way that can be
        passed to `procmod.ProcessMonitor`.

        Parameters:
         program (str): program to execute 
         option (list of str): option to be added to the program, for example, ['-b', '1', '--advanced']
         useConfig (bool): if true, "-e path/to/chillerconfig.json" will be appended to command
        """
        raise Exception('Runner must implement `command`!')

class LabRemoteRunner(ICommandRunner):
    """
    Executes the `chiller_cycle` program from the labRemote
    package.
    """
    def __init__(self, chillerconf='', labRemote='/usr', streamName='Climate'):
        """
        Parameters:
         chillerconf (str): location of the hardware configuration file
         labRemote (str): Path to labRemote installation.
        """
        super().__init__()

        # Store configuration
        self.chillerconf=chillerconf
        self.labRemote=labRemote
        self.streamName=streamName

    def command(self, program='chiller_cycle', option=None, useConfig=True, save_influx=True, save_csv=True, debug=0):
        cmd=[]
        cmd+=['{prefix}/bin/{exe}'.format(prefix=self.labRemote, exe=program)]

        if useConfig:
            cmd+=['-e', self.chillerconf]

        cmd+=['-n', 'CH1']

        if save_influx:
            cmd+=['-s', self.streamName]
        if save_csv:
            cmd+=['-o', "chillerMon"]

        if debug>0:
            cmd+=['-'+'d'*debug]
        
        if option != None:
            cmd+=option

        return cmd

