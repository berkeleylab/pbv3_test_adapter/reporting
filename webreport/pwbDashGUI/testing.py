import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_table
from dash_table.Format import Format, Scheme, Sign, Symbol
from dash.dependencies import Input, Output, State, ClientsideFunction
from dash.exceptions import PreventUpdate

import plotly.graph_objects as go
from time import time, sleep
import codecs
import pickle
import datetime
import pkg_resources

import plotly.graph_objects as go
import pandas as pd
import glob

from webreport.pwbDashCommon import template

from . import panel

from database import pwbdb
import os

import itkdb
import datetime

import threading


def parseContent(inString, keyword):
    key1 = keyword + ':---> '
    key2 = ' <---:'+keyword
    idx_begin = inString.rfind(key1)
    idx_end = inString.rfind(key2)
    if idx_begin == -1 or idx_end == -1:
        return ""
    return inString[idx_begin+len(key1):idx_end]

def pad_dataframe_pbs(df):
    """
    Fill a dataframe with powerboards that don't exist in it.
    """
    allpbs=set(range(0,10))
    pbs=set(df.pb.unique())
    misspbs=allpbs-pbs
    return df.append([{'pb': pb} for pb in misspbs]).sort_values('pb')

def pivot_ven_dataframe(df, oncol, invert=False):
    """
    Rotates a dataframe from a VEnParser into a table
    format.

    df: Original dataframe
    oncol: Name of column indicating on/off state
    invert: oncol==0 means ON
    """
    df=df.copy()
    str0 = "OFF" if not invert else "ON"
    str1 = "ON"  if not invert else "OFF"
        
    df['suffix']=df.apply(lambda x: str1 if x[oncol] else str0, axis=1, result_type='reduce')
    df=df.set_index(['pb','suffix']).unstack(-1)
    df.columns=df.columns.map(''.join)
    return df.reset_index()

def hex_to_string(hex_string):
    return ''.join(chr(int(hex_string[i:i+2], 16)-1) for i in range(0, len(hex_string), 2))

class PBv3TableGen:
    def __init__(self, tableid):
        self.tableid=tableid

        self.coldef  =[{'name':['','Powerboard'],'id':'pb'}]
        self.condform=[{'if':{'column_id':'pb'},'width':'50px'}]        


    def add_columns(self,columns):
        for column in columns:
            if type(column)!=dict:
                column={
                    'id': column,
                    'name': column
                    }
            if type(column["name"])!=tuple:
                name=('',column["name"])
            else:
                name=column['name']
                

            self.coldef+=[
                {'name':name,
                     'id':f'value_{column["id"]}',
                     'type':'numeric',
                     'format': Format(
                         precision=column.get('precision',2),
                         scheme=column.get('scheme',Scheme.fixed))
                }
            ]

            self.condform+=[
                {
                    'if': {
                        'column_id': f'value_{column["id"]}',
                        'filter_query': f'{{error_{column["id"]}}} = 0'
                        },
                        'backgroundColor': 'lightgreen'
                },
                {
                    'if': {
                        'column_id': f'value_{column["id"]}',
                        'filter_query': f'{{error_{column["id"]}}} = 1'
                        },
                        'backgroundColor': 'salmon'
                }
                ]

    def add_onoff_columns(self,columns):
        for column in columns:
            if type(column)!=dict:
                column={
                    'id': column,
                    'name': column
                    }
            
            self.coldef+=[
                {'name':[f'{column["name"]} [{column.get("units","V")}]','OFF'],
                     'id':f'value_{column["id"]}OFF',
                     'type':'numeric',
                     'format': Format(
                         precision=2,
                         scheme=column.get('scheme',Scheme.fixed))
                },
                {'name':[f'{column["name"]} [{column.get("units","V")}]','ON'],
                     'id':f'value_{column["id"]}ON',
                     'type':'numeric',
                     'format': Format(
                         precision=2,
                         scheme=column.get('scheme',Scheme.fixed))
                }
                ]

            self.condform+=[
                {
                    'if': {
                        'column_id': f'value_{column["id"]}OFF',
                        'filter_query': f'{{error_{column["id"]}OFF}} = 0'
                        },
                        'backgroundColor': 'lightgreen'
                },
                {
                    'if': {
                        'column_id': f'value_{column["id"]}OFF',
                        'filter_query': f'{{error_{column["id"]}OFF}} = 1'
                        },
                        'backgroundColor': 'salmon'
                },
                {
                    'if': {
                        'column_id': f'value_{column["id"]}ON',
                        'filter_query': f'{{error_{column["id"]}ON}} = 0'
                        },
                        'backgroundColor': 'lightgreen'
                },
                {
                    'if': {
                        'column_id': f'value_{column["id"]}ON',
                        'filter_query': f'{{error_{column["id"]}ON}} = 1'
                        },
                        'backgroundColor': 'salmon'
                },
                ]

    def generate(self):
        return dash_table.DataTable(
            id=self.tableid,
            merge_duplicate_headers=True,
            columns=self.coldef,
            style_data_conditional=self.condform,
            style_header={'fontWeight': 'bold'},
            style_cell={'width':'85px'}
            )            

class TestingPage:
    def __init__(self, cfg, app, port=5000):
        #
        # Run stuff
        self.cfg=cfg

        self.port=port

        self.panels=[]
        panellabels=[]
        self.df_temp=[]
        self.nTabs=len(cfg.panels)

        if self.cfg.runAutoQC:
            t = threading.Thread(target=self.run_background_task)
            t.start()

        self.command_dir = ""
        if cfg.autoQC_cfg is not None:
            self.command_dir = cfg.autoQC_cfg['command_dir']

        for panelcfg in cfg.panels:
            panelcfg=panelcfg.copy()
            name  =panelcfg.pop('name'  )
            runner=panelcfg.pop('runner')

            panelobj=getattr(panel,runner)(**panelcfg)

            self.panels.append(panelobj)
            panellabels.append(name)
            df_temp_thispanel=[]
            for pb in range(10):
                df_temp_thispanel.append(pd.DataFrame({"time":[], "NTCpb":[], "PTAT":[], "CTAT":[]}))
            self.df_temp.append(df_temp_thispanel)

        #
        # Layout related stuff
        pbchecks=[
            dcc.Checklist(id=f'pb-{pb}',
                          options = [{'label': 'Board {}'.format(pb), 'value': pb}])
            for pb in range(10)]

        runradios=[
            dcc.RadioItems(id='debug', options = [
                {'label': 'None'       , 'value': 0},
                {'label': 'Debug'      , 'value': 1},
                {'label': 'Super Debug', 'value': 4}
                ], value=0)
            ]

        rundropdowns=[
            dcc.Dropdown(id='transition-dropdown', options = [
            {'label': 'SMD Loading', 'value': 'SMD_LOAD'},
            {'label': 'Coil and Shield Loading', 'value': 'MAN_LOAD'},
            {'label': 'Die Attachment and Bonding', 'value': 'BONDED'},
            {'label': 'Thermal Cycling', 'value': 'THERMAL'},
            {'label': 'Burn-In', 'value': 'BURN_IN'},
            {'label': 'Module Reception',   'value': 'MODULE_RCP'},
            {'label': 'Loaded on a Module', 'value': 'LOADED'},
            {'label': 'Loaded on a Hybrid Burn-in Carrier', 'value': 'HYBBURN'}
            ], value='MODULE_RCP'),
            dcc.Dropdown(id='specify-test-tag-dropdown', options = [
            {'label': 'BONDED warm', 'value': 'BONDED warm'},
            {'label': 'BONDED cold', 'value': 'BONDED cold'},
            {'label': 'THERMAL warm', 'value': 'THERMAL warm'},
            {'label': 'THERMAL cold', 'value': 'THERMAL cold'},
            {'label': 'BURN_IN warm', 'value': 'BURN_IN warm'},
            {'label': 'BURN_IN cold', 'value': 'BURN_IN cold'}
            ], value='', placeholder="Set a tag for this test from dropdown menu")
            ]

        transitionchecks=[
            dcc.Checklist(id='force-transition', options = [
            {'label' : 'force', 'value' : 1}
            ], value = [1])
            ]

        tempMonitorChecks=[
            dcc.Checklist(id='save-csv',    options = [{'label' : 'Save to CSV', 'value' : 1}], value = [1]),
            dcc.Checklist(id='save-influx', options = [{'label' : 'Save to InfluxDB', 'value' : 1}], value = []),
            dcc.Checklist(id='add-NTCpb',   options = [{'label' : 'NTCpb',  'value' : 1}], value = [1]),
            dcc.Checklist(id='add-PTAT',    options = [{'label' : 'PTAT',   'value' : 1}], value = [1]),
            dcc.Checklist(id='add-CTAT',    options = [{'label' : 'CTAT',   'value' : 1}], value = [1]),
            dcc.Checklist(id='add-chiller', options = [{'label' : 'chiller','value' : 1}], value = [1])
            ]


        # Tables
        table_ven  = PBv3TableGen('LVEn-table')
        table_ven.add_columns([
            {'id':'PADID','name':'Pad ID','precision':0},
            {'id':'RELIABILITY','name':'BER','precision':2},
            ])
        table_ven.add_onoff_columns([
            'linPOLV',
            {'id':'VOUT','name':'DC/DC Out'},
            {'id':'HVIIN'    , 'name':'HV In Current' ,'units':'A','precision':2,'scheme':Scheme.decimal_or_exponent},
            {'id':'HVIOUT'   , 'name':'HV Out Current','units':'A','precision':2,'scheme':Scheme.decimal_or_exponent},
            {'id':'AMACHVRET', 'name':'HVret'         ,'units':'counts','precision':0},            
            ])

        table_tio1 = PBv3TableGen('tio-table-1')
        table_tio1.add_onoff_columns(['OFout', 'CALx', 'CALy', 'Shuntx', 'Shunty'])

        table_tio2 = PBv3TableGen('tio-table-2')
        table_tio2.add_onoff_columns(['LDx0EN', 'LDx1EN', 'LDx2EN', 'LDy0EN', 'LDy1EN', 'LDy2EN'])

        table_temp = PBv3TableGen('temp-table')
        table_temp.add_columns([
            {'id':'NTCx' ,'name':'NTCx [cnt]' ,'precision':0},
            {'id':'NTCy' ,'name':'NTCy [cnt]' ,'precision':0},
            {'id':'NTCpb','name':'NTCpb [cnt]','precision':0},
            {'id':'CTAT' ,'name':'CTAT [cnt]' ,'precision':0},
            {'id':'PTAT' ,'name':'PTAT [cnt]' ,'precision':0},
        ])

        table_dcdcadj = PBv3TableGen('dcdcadj-table')
        table_dcdcadj.add_columns([
            {'id':'minusThirteen' ,'name':'-13%','precision':1},
            {'id':'minusSix'      ,'name':'-6%' ,'precision':1},
            {'id':'plusSix'       ,'name':'+6%' ,'precision':1},
        ])
        
        table_dcdc = PBv3TableGen('eff-table')
        table_dcdc.add_columns([
            {'id':'EFF2A','name':('2A Load','Efficiency'),'precision':2},
            {'id':'AMACPTAT2A','name':('2A Load','AMAC NTC PB [counts]'),'precision':0}
            ])

        tables=[
            table_ven    .generate(),
            table_tio1   .generate(),
            table_tio2   .generate(),
            table_temp   .generate(),
            table_dcdcadj.generate(),
            table_dcdc   .generate()
            ]

        # Create the layout
        self.layout = template.jinja2_to_dash(
            pkg_resources.resource_filename(__name__,'template'),
            'testing.html', replace=pbchecks+transitionchecks+tempMonitorChecks+runradios+rundropdowns+tables, panels=panellabels)

        #
        # Callbacks

        ## Server-side Callbacks
        app.callback(
            [
                Output('panel-dbresult', 'data'),
                Output('auto-fill-msg', 'children'),
            ],
            Input('panel-populate-button', 'n_clicks'),
            [
                State('panel-number', 'value'),
                State('itkdb_auth', 'data'),
                State('itkdb_auth_pass1', 'data'),
                State('itkdb_auth_pass2', 'data')
            ],
            prevent_initial_call=True
            )(self.populateFromDB)
 
        app.callback(
            Output('upload-test-msg', 'children'),
            [Input('panel-upload-test-result-button', 'n_clicks')],
            [
                State('tabs', 'value'),
                State('itkdb_auth_pass1', 'data'),
                State('itkdb_auth_pass2', 'data'),
                State('panel-number', 'value'),
                State('version', 'value'),
                State('batch', 'value'),
                State('pb-0', 'value'),
                State('pb-1', 'value'),
                State('pb-2', 'value'),
                State('pb-3', 'value'),
                State('pb-4', 'value'),
                State('pb-5', 'value'),
                State('pb-6', 'value'),
                State('pb-7', 'value'),
                State('pb-8', 'value'),
                State('pb-9', 'value'),
                State('serial-0', 'value'),
                State('serial-1', 'value'),
                State('serial-2', 'value'),
                State('serial-3', 'value'),
                State('serial-4', 'value'),
                State('serial-5', 'value'),
                State('serial-6', 'value'),
                State('serial-7', 'value'),
                State('serial-8', 'value'),
                State('serial-9', 'value')
            ]
            )(self.uploadTestResults)

        app.callback(
            Output('panel-transition-msg', 'children'),
            [Input('panel-transition-button', 'n_clicks')],
            [
                State('transition-dropdown', 'value'),
                State('force-transition', 'value'),
                State('tabs', 'value'),
                State('itkdb_auth_pass1', 'data'),
                State('itkdb_auth_pass2', 'data'),
                State('panel-number', 'value')
            ]
            )(self.panelStageTransition)
 
        app.callback(
            Output('upload-status-msg', 'children'),
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
        )(self.updateUploadTestResultsStatus)

        app.callback(
            Output('transition-status-msg', 'children'),
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
        )(self.updateTransitionStatus)
      
        app.callback(
            Output('active-test-msg', 'children'),
            [Input('active-test-button', 'n_clicks')],
            [
                State('tabs', 'value'),
                State('debug', 'value'),
            ]
            )(self.runStandBy)


        app.callback(
            Output('basic-test-msg', 'children'),
            [Input('basic-test-button', 'n_clicks')],
            [
                State('tabs', 'value'),
                State('debug', 'value'),
                State('specify-test-tag', 'value'),
                State('specify-test-tag-dropdown', 'value'),
                State('panel-number', 'value'),
                State('version', 'value'),
                State('batch', 'value'),
                State('pb-0', 'value'),
                State('pb-1', 'value'),
                State('pb-2', 'value'),
                State('pb-3', 'value'),
                State('pb-4', 'value'),
                State('pb-5', 'value'),
                State('pb-6', 'value'),
                State('pb-7', 'value'),
                State('pb-8', 'value'),
                State('pb-9', 'value'),
                State('serial-0', 'value'),
                State('serial-1', 'value'),
                State('serial-2', 'value'),
                State('serial-3', 'value'),
                State('serial-4', 'value'),
                State('serial-5', 'value'),
                State('serial-6', 'value'),
                State('serial-7', 'value'),
                State('serial-8', 'value'),
                State('serial-9', 'value')
            ]
            )(self.runBasicTests)

        app.callback(
            Output('advanced-test-msg', 'children'),
            [Input('advanced-test-button', 'n_clicks')],
            [
                State('tabs', 'value'),
                State('debug', 'value'),
                State('specify-test-tag', 'value'),
                State('specify-test-tag-dropdown', 'value'),
                State('panel-number', 'value'),
                State('version', 'value'),
                State('batch', 'value'),
                State('pb-0', 'value'),
                State('pb-1', 'value'),
                State('pb-2', 'value'),
                State('pb-3', 'value'),
                State('pb-4', 'value'),
                State('pb-5', 'value'),
                State('pb-6', 'value'),
                State('pb-7', 'value'),
                State('pb-8', 'value'),
                State('pb-9', 'value'),
                State('serial-0', 'value'),
                State('serial-1', 'value'),
                State('serial-2', 'value'),
                State('serial-3', 'value'),
                State('serial-4', 'value'),
                State('serial-5', 'value'),
                State('serial-6', 'value'),
                State('serial-7', 'value'),
                State('serial-8', 'value'),
                State('serial-9', 'value')
            ]
            )(self.runAllTests)        

        app.callback(
            Output('stop-msg', 'children'),
            [Input('stop-button', 'n_clicks')],
            [
                State('tabs', 'value'),
            ]
            )(self.stopRun)        

        app.callback(
            Output('burnin_message', 'children'),
            [Input('burnin_button', 'n_clicks')],
            [
                State('tabs', 'value'),
                State('debug', 'value'),
                State('burnin_load', 'value'),
                State('burnin_time', 'value'),
                State('pb-0', 'value'),
                State('pb-1', 'value'),
                State('pb-2', 'value'),
                State('pb-3', 'value'),
                State('pb-4', 'value'),
                State('pb-5', 'value'),
                State('pb-6', 'value'),
                State('pb-7', 'value'),
                State('pb-8', 'value'),
                State('pb-9', 'value')
            ]
            )(self.runBurnIn)        
       
        app.callback(
            [
                Output('display-test', 'children'),
            ],
            [Input('tabs','value'), Input('interval-component-run', 'n_intervals')]
        )(self.updateRunData)
        
        app.callback(
            Output('active-result-msg', 'children'),
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
        )(self.updateStandByData)

        app.callback(
            [
                Output('LVEn-table', 'data'),
                Output('ven-last-update', 'data')
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('ven-last-update', 'data')]
        )(self.updateVEndata)
        
        app.callback(
            [
                Output('tio-data', 'data'),
                Output('tio-last-update', 'data')
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('tio-last-update', 'data')]
        )(self.updateTIOData)

        app.callback(
            [
                Output('temp-table', 'data'),
                Output('temp-last-update', 'data')
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('temp-last-update', 'data')]
        )(self.updateTempData)

        app.callback(
            [
                Output('dcdcadj-table', 'data'),
                Output('dcdcadj-last-update', 'data')
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('dcdcadj-last-update', 'data')]
        )(self.updateDCDCAdjData)        

        app.callback(
            [
                Output('lviv-data', 'data'),
                Output('lviv-last-update', 'data'),
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('lviv-last-update', 'data')]
        )(self.updateLVIVdata)

        app.callback(
            [
                Output('amsl-data', 'data'),
                Output('amsl-last-update', 'data'),
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('amsl-last-update', 'data')]
        )(self.updateAMSlopedata)

        app.callback(
            [
                Output('dac-data', 'data'),
                Output('dac-last-update', 'data'),
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('dac-last-update', 'data')]
        )(self.updateDACdata)

        app.callback(
            [
                Output('eff-table', 'data'),
                Output('dcdc-data', 'data'),
                Output('dcdc-last-update', 'data'),
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('dcdc-last-update', 'data')]
        )(self.updateDCDCdata)

        app.callback(
            [
                Output('hvrt-data', 'data'),
                Output('hvrt-last-update', 'data'),
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('hvrt-last-update', 'data')]
        )(self.updateHVdata)

        app.callback(
            Output('panel-status', 'data'),
            [Input('interval-component-status', 'n_intervals')]
        )(self.updateRunStatus)

        app.callback(
            Output('start_pb_monitor_message', 'children'),
            [Input('start_pb_monitor_button','n_clicks')],
            [
                State('tabs', 'value'),
                State('debug', 'value'),
                State('save-csv', 'value'),
                State('save-influx', 'value'),
                State('pb-0', 'value'),
                State('pb-1', 'value'),
                State('pb-2', 'value'),
                State('pb-3', 'value'),
                State('pb-4', 'value'),
                State('pb-5', 'value'),
                State('pb-6', 'value'),
                State('pb-7', 'value'),
                State('pb-8', 'value'),
                State('pb-9', 'value')
            ],
            prevent_initial_call=True)(self.runStartPBMonitor)

        app.callback(
            Output('stop_pb_monitor_message', 'children'),
            [Input('stop_pb_monitor_button', 'n_clicks')],
            [
                State('tabs', 'value'),
            ]
            )(self.runStopPBMonitor)        

        app.callback(
            Output('clear_pb_monitor_data_message', 'children'),
            [Input('clear_pb_monitor_data_button','n_clicks')],
            [
                State('tabs', 'value'),
            ],
            prevent_initial_call=True)(self.runClearPBMonitor)

        app.callback(
            Output('pb-temp-monitor-graph', 'figure'),
            [
                Input('interval-component-run', 'n_intervals'), 
                Input('tabs','value'),
                State('add-NTCpb', 'value'),
                State('add-PTAT', 'value'),
                State('add-CTAT', 'value'),
                State('add-chiller', 'value'),
                State('pb-0', 'value'),
                State('pb-1', 'value'),
                State('pb-2', 'value'),
                State('pb-3', 'value'),
                State('pb-4', 'value'),
                State('pb-5', 'value'),
                State('pb-6', 'value'),
                State('pb-7', 'value'),
                State('pb-8', 'value'),
                State('pb-9', 'value')
            ],
        )(self.updatePBTempGraph)

        ## Client-side Callbacks

        # Store checkbox settings per panel
        panelstoreelements=['panel-number', 'version', 'batch']
        panelstoreelements+=[f'pb-{pb}' for pb in range(10)]
        panelstoreelements+=[f'serial-{pb}' for pb in range(10)]
        panelstoreelements+=['debug']

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='storePanelInfo'
                ),
            Output('panel-storage', 'data'),
            [Input(name, 'value') for name in panelstoreelements],
            [State('panel-storage', 'data'), State('tabs','value')])

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='restorePanelInfo'
                ),
            [Output(name, 'value') for name in panelstoreelements],
            [Input('tabs', 'value'), Input('panel-dbresult', 'data')],
            [State('panel-storage', 'data')])
        
        # Update status styles
        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateTabColors'
                ),
            [o for p in range(len(self.panels)) for o in [Output(f'tab-{p}', 'style'),Output(f'tab-{p}', 'selected_style')]],
            [Input('panel-status', 'data')],
            prevent_initial_call=True
            )

        # Graph updates
        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateTIOtable'
                ),
                [
                    Output('tio-table-1','data'),
                    Output('tio-table-2','data')
                ],
                [Input('tio-data', 'data')]
            )

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateLVIVgraph'
                ),
                Output('lviv-graph','figure'),
                [Input('lviv-data', 'data')]
            )

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateAMSlopegraph'
                ),
                Output('amsl-graph','figure'),
                [Input('amsl-data', 'data')]
            )

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateDACgraph'
                ),
                [
                    Output('shuntx-graph' ,'figure'),
                    Output('shunty-graph','figure'),
                    Output('calx-graph','figure'),
                    Output('caly-graph','figure')
                ],
                [Input('dac-data', 'data')]
            )

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateDCDCgraph'
                ),
                [
                    Output('eff-graph' ,'figure'),
                    Output('temp-graph','figure'),
                    Output('icur-graph','figure'),
                    Output('ocur-graph','figure')
                ],
                [Input('dcdc-data', 'data')]
            )

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateHVretgraph'
                ),
                [
                    Output('gain0-graph','figure'),
                    Output('gain1-graph','figure'),
                    Output('gain2-graph','figure'),
                    Output('gain4-graph','figure')
                ],
                [Input('hvrt-data', 'data')]
            )

    def populateFromDB(self, button, panel, user, pass1, pass2):
        """
        Query the database for Powerboard ID's on a panel and return
        the values in a dictionary.

        If an error occurs (invalid login, mixing version/batch numbers),
        then an exception is thrown.

        Dictionary Keys:
         - version: Version number
         - batch: Batch number
         - pbEnable: List of 10 booleans, true if the Powerboard is preset
         - pbNumber: List of 10 integers, corresponding to Powerboard number
        """
        if panel == '':
            return None, ' Please provide a panel number'
        if user==None:
            return None, ' Please login to ITk DB first!!!'

        # Create client
        user=pickle.loads(codecs.decode(user.encode(),'base64'))

        if (not user.is_authenticated()) or (pass1 is None) or (pass2 is None):
            return None, ' Please login to ITk DB first!!!'
        if user.is_expired() or user.expires_in < 300:
            print('IDk DB login is expired or is expiring, re-login')
            user = itkdb.core.User(pass1, pass2)
            user.authenticate()
        if not user.is_authenticated():
            return None, ' Please login to ITk DB first!!!'

        client=itkdb.Client(user=user)

        #
        # Query the database
        result={'pbEnable':[False]*10, 'pbNumber':[0]*10}
        carrier=pwbdb.Component(f'20USBPC{panel}', client)
        componentType = ""
        try:
            componentType = carrier.componentType
        except:
            componentType = "InValid"
        if componentType != "PWB_CARRIER":
            return None, ' Panel not found in DB. Please provide a valid panel number!!!'

        for pbNum,pb in carrier.children.get('PWB',{}).items():
            # Update common values
            if result.get('version', pb.property('VERSION'))!=pb.property('VERSION'):
                return None, ' Cannot mix version numbers'
            result['version']=pb.property('VERSION')

            if result.get('batch', pb.property('BATCH'))!=pb.property('BATCH'):
                return None, ' Cannot mix batch numbers'
            result['batch']=pb.property('BATCH')
            if pb.currentStage == "FAILED":
                continue

            # Enable powerboard
            result['pbEnable'][pbNum]=True
            result['pbNumber'][pbNum]=pb.property('PB_NUMBWE')

        return result, ' '

    def uploadTestResults_execute(self, tab, pass1, pass2, args):
        """
        Execute: Upload test results to idkdb
        """
        panel  =args[0].strip()
        if panel == '':
            return ' Please provide a panel number!'

        if args[1] == '':
            return ' Please provide a powerboard version number!'
        version=int(args[1])

        if args[2] == '':
            return ' Please provide a powerboard batch number!'
        batch  =int(args[2])

        boards =sum(args[3:13],[])
        serials=map(args[13:23].__getitem__, boards)


        if len(boards)==0:
            return 'No boards selected!'


        # Create client
        if (pass1 is None) or (pass2 is None):
            return ' Please login to ITk DB first!!!'

        user = itkdb.core.User(pass1, pass2)
        user.authenticate()

        if not user.is_authenticated():
            return ' Please login to ITk DB first!!!'

        if user.is_expired() or user.expires_in < 300:
            print('IDk DB login is expired or is expiring, re-login')
            user = itkdb.core.User(pass1, pass2)
            user.authenticate()

        if not user.is_authenticated():
            return ' Please login to ITk DB first!!!'

        client=itkdb.Client(user=user)

        msg = ""
        try:
            msg = self.panels[int(tab)].runUploadData(client, version, batch, boards, serials, panelName=panel)
        except:
            return sys.exc_info()[0]
        
        return msg

    def uploadTestResults(self, button, tab, pass1, pass2, *args):
        """
        Upload test results to idkdb
        """
        if button==None:
            raise PreventUpdate
        return self.uploadTestResults_execute(tab, pass1, pass2, args)

    def panelStageTransition_execute(self, stage, force, tab, pass1, pass2, args):
        """
        Execute: Upload test results to idkdb
        """
        panel  =args[0].strip()

        if panel == '':
            return ' Please provide a panel number!'
        if stage == None:
            return ' Please select a stage to transition to!'

        # Create client
        if (pass1 is None) or (pass2 is None):
            return ' Please login to ITk DB first!!!'

        user = itkdb.core.User(pass1, pass2)
        user.authenticate()

        if not user.is_authenticated():
            return ' Please login to ITk DB first!!!'

        if user.is_expired() or user.expires_in < 300:
            print('IDk DB login is expired or is expiring, re-login')
            user = itkdb.core.User(pass1, pass2)
            user.authenticate()
        if not user.is_authenticated():
            return ' Please login to ITk DB first!!!'

        client=itkdb.Client(user=user)

        force_ = False
        if len(force) > 0:
            force_ = True

        msg = ""

        try:
            msg = self.panels[int(tab)].runPanelTransition(client, panel, stage, force_)
        except:
            return sys.exc_info()[0]

        return msg

    def panelStageTransition(self, button, stage, force, tab, pass1, pass2, *args):
        """
        Upload test results to idkdb
        """
        if button==None:
            raise PreventUpdate

        return self.panelStageTransition_execute(stage, force, tab, pass1, pass2, args)

    def runStandBy_execute(self, tab, debug, args):
        """
        Execute: Run system initialization
        """
        if self.panels[int(tab)].running:
            return 'Test program already running!'

        try:
            self.panels[int(tab)].runGeneral(program='pbv3_stand_by', option=None, useConfig=True)
        except:
            return sys.exc_info()[0]

        return ''

    def runStandBy(self, button, tab, debug, *args):
        """
        Run system initialization
        """
        if button==None:
            raise PreventUpdate

        return self.runStandBy_execute(tab, debug, args)

    def runBasicTests_execute(self, tab, debug, testTag_custom, testTag_dropdown, args):
        """
        Execute: Run only the basic set of tests.
        """
        panel  =args[0].strip()
        version=int(args[1])
        batch  =int(args[2])
        boards =sum(args[3:13],[])
        serials=map(args[13:23].__getitem__, boards)

        if self.panels[int(tab)].running:
            return 'Test program already running!'

        if panel=='':
            return 'Must specify a panel name!'

        if len(boards)==0:
            return 'No boards selected'
        
        testTag = ""
        if testTag_custom != "":
            testTag = testTag_custom
        elif testTag_dropdown is not None:
            testTag = testTag_dropdown
        else:
            testTag = ""
        testTag = testTag.replace(" ", "_")

        try:
            self.panels[int(tab)].run(version, batch, boards, serials, panelName=panel, institution=self.cfg.institution, debug=debug, basic=True, testTag=testTag)
        except:
            return sys.exc_info()[0]

        return ''

    def runBasicTests(self, button, tab, debug, testTag_custom, testTag_dropdown, *args):
        """
        Run only the basic set of tests.
        """
        if button==None:
            raise PreventUpdate

        return self.runBasicTests_execute(tab, debug, testTag_custom, testTag_dropdown, args)

    def runAllTests_execute(self, tab, debug, testTag_custom, testTag_dropdown, args):
        """
        Execute: Run the full suit of tests.
        """
        panel  =args[0].strip()
        version=args[1].strip()
        batch  =int(args[2])
        boards =sum(args[3:13],[])
        serials=map(args[13:23].__getitem__, boards)
        
        if self.panels[int(tab)].running:
            return 'Test program already running!'

        if panel=='':
            return 'Must specify a panel name!'

        if len(boards)==0:
            return 'No boards selected'

        testTag = ""
        if testTag_custom != "":
            testTag = testTag_custom
        elif testTag_dropdown is not None:
            testTag = testTag_dropdown
        else:
            testTag = ""
        testTag = testTag.replace(" ", "_")

        self.panels[int(tab)].run(version, batch, boards, serials, panelName=panel, institution=self.cfg.institution, debug=debug, basic=False, testTag=testTag)

        return ''

    def runAllTests(self, button, tab, debug, testTag_custom, testTag_dropdown, *args):
        """
        Run the full suit of tests.
        """
        if button==None:
            raise PreventUpdate

        return self.runAllTests_execute(tab, debug, testTag_custom, testTag_dropdown, args)

    def stopRun_execute(self, tab):
        """
        Execute: Stop any running tests
        """
        if not self.panels[int(tab)].running:
            return 'Test program not running!'

        self.panels[int(tab)].stop()

        return ''

    def stopRun(self, button, tab):
        """
        Stop any running tests
        """
        if button==None:
            raise PreventUpdate

        return self.stopRun_execute(tab)

    def runBurnIn_execute(self, tab, debug, load, time, args):
        """
        Execute: Run burn in
        """
        boards =sum(args[0:10],[])
        if len(boards)==0:
            return 'No boards selected!'

        if self.panels[int(tab)].running:
            return 'Test program already running!'

        self.panels[int(tab)].runBurnInCommand(boards, debug=debug, load=float(load), time=float(time))

        return ''

    def runBurnIn(self, button, tab, debug, load, time, *args):
        """
        Run burn in
        """
        if button==None:
            raise PreventUpdate

        return self.runBurnIn_execute(tab, debug, load, time, args)
   
    def updateRunStatus(self, interval):
        return [ panel.running for panel in self.panels]
        #return [ panel.running for panel in self.panels]

    def updateRunData(self, tab, interval):
        # Parse new data
        start=time()
        panel=self.panels[int(tab)]
        panel.parse()

        # Terminal output
        terminal = '\n'.join(panel.data)

        return [terminal]

    def updateTempData(self, interval, tab, lastUpdate):
        # Parse new data
        start=time()
        panel=self.panels[int(tab)]        
        panel.parse()

        if type(lastUpdate)==dict and (lastUpdate['tab']==tab
                                           and lastUpdate['timestamp']>=panel.temp.lastUpdate
                                           ): # no new data
            raise PreventUpdate

        lastUpdate=max([panel.temp.lastUpdate])

        #
        # Empty dataframe with data
        tbl = pd.DataFrame(data={'pb':range(10)})        

        # Temperature
        tbl=tbl.merge(panel.temp.data, on='pb', how='outer')

        # Final table
        tbl=tbl.to_dict('records')

        return tbl, {'tab': tab, 'timestamp': lastUpdate}

    def updateDCDCAdjData(self, interval, tab, lastUpdate):
        # Parse new data
        panel=self.panels[int(tab)]        
        panel.parse()

        if type(lastUpdate)==dict and (lastUpdate['tab']==tab
                                           and lastUpdate['timestamp']>=panel.dcad.lastUpdate
                                           ): # no new data
            raise PreventUpdate

        lastUpdate=max([panel.dcad.lastUpdate])

        #
        # Empty dataframe with data
        tbl = pd.DataFrame(data={'pb':range(10)})        

        # DCDC Adjust        
        tbl=tbl.merge(panel.dcad.data, on='pb', how='outer')

        # Turn into %
        for column in tbl.columns:
            if not column.startswith('value'):
                continue
            tbl[column]=tbl[column]*100

        # Final table
        tbl=tbl.to_dict('records')

        return tbl, {'tab': tab, 'timestamp': lastUpdate}

    def updateStandByData(self, interval, tab):
        # Parse new data
        panel=self.panels[int(tab)]
        panel.parse()

        # Terminal output
        post_msg = ''
        panel_str = ''.join(panel.data)
        #clear message once main tests started
        if "Performing PS and active board checks" not in panel_str:
            return ''
        else:
            post_msg = 'Waiting for PS and I2C check result... '
        if "PSINIT passed" in panel_str:
            post_msg = 'PS initialization OK. '
        if "PSINIT failed" in panel_str:
            post_msg = 'PS init FAILED!! '
        if "I2CDEV passed" in panel_str:
            post_msg += ' I2C check OK. '
        if "I2CDEV failed" in panel_str:
            post_msg += ' I2C check FAILED!! '
        if "PS initialization OK" in post_msg and "I2C check OK" in post_msg:
            post_msg += ' System is ready for tests. '

        return post_msg

    def updateUploadTestResultsStatus(self, interval, tab):
        # Parse new data
        panel=self.panels[int(tab)]

        # Terminal output
        post_msg = ''

        if len(panel.dbUploadString.contents) > 0:
            post_msg = 'Upload finished for '+str(len(panel.dbUploadString.contents)) + ' PBs ('+str(panel.dbUploadString.nContents)+' files).'
            if panel.dbUploadString.hasError:
                post_msg += ' Some uploads failed, see terminal output.'
            if panel.dbUploadString.hasWarning:
                post_msg += ' Some uploads have warning, see terminal output.'

        return post_msg

    def updateTransitionStatus(self, interval, tab):
        # Parse new data
        panel=self.panels[int(tab)]
        panel.parse()

        # Terminal output
        panel_str = ''.join(panel.data)

        #clear message once main tests started
        if "===>Transition panel" not in panel_str:
            return ''

        if '===>Transition finished for panel' in panel_str:
            return 'Transition finished!'
        if '===>ILLEGAL_STAGE' in panel_str:
            return 'Illegal stege! Check terminal output.'
        if '===>NO_TEST' in panel_str or '===>MISSING_TEST' in panel_str or '===>FAILED_TEST' in panel_str:
            return 'Missing/failed tests in current stage! Check terminal output.'

        return ''

    def updateVEndata(self, interval, tab, lastUpdate):
        # Parse new data
        start=time()
        panel=self.panels[int(tab)]        
        panel.parse()

        lastUpdatePanel=max([panel.padi.lastUpdate, panel.bert.lastUpdate, panel.lven.lastUpdate, panel.hven.lastUpdate])
        if type(lastUpdate)==dict and (lastUpdate['tab']==tab
                                       and lastUpdate['timestamp']>=lastUpdatePanel): # no new data
            raise PreventUpdate

        
        #
        # Random voltage on/off and other checks
        ven = pd.DataFrame(data={'pb':range(10)})        

        # Pad ID
        ven=ven.merge(panel.padi.data, on='pb', how='outer')

        # BER
        ven=ven.merge(panel.bert.data, on='pb', how='outer')

        # Reformat LV_ENABLE
        if len(panel.lven.data.index)>0:
            dlven=pivot_ven_dataframe(panel.lven.data, 'DCDCen')
            ven=ven.merge(dlven, on='pb', how='outer')

        # Reformat HV_ENABLE
        if len(panel.hven.data.index)>0:
            dhven=pivot_ven_dataframe(panel.hven.data, 'CntSetHV0en')
            ven=ven.merge(dhven, on='pb', how='outer')

        # Reformat OFin test
        if len(panel.ofin.data.index)>0:
            dofin=pivot_ven_dataframe(panel.ofin.data, 'OF', True)
            ven=ven.merge(dofin, on='pb', how='outer')

        # Final table
        ven=ven.to_dict('records')

        return ven, {'tab': tab, 'timestamp': lastUpdatePanel}
    
    def updateTIOData(self, interval, tab, lastUpdate):
        # Parse new data
        panel=self.panels[int(tab)]
        panel.parse()

        if type(lastUpdate)==dict and (lastUpdate['tab']==tab and lastUpdate['timestamp']==panel.tout.lastUpdate): # no new data
            raise PreventUpdate


        # Toggle output tables
        tio=pd.DataFrame(data={'pb':range(10)})

        tout=panel.tout.data
        if len(tout.index)>0:
            tout['name']=tout[['output','state']].apply(lambda x: x['output']+('ON' if x['state'] else 'OFF'), axis=1, result_type='reduce')
            tout=tout.set_index(['pb','name'])[['error','value']].unstack(-1)
            tout.columns=tout.columns.map('_'.join)

        tio=tio.merge(tout,on='pb',how='outer')
        tio=tio.to_dict('records')

        return tio, {'tab': tab, 'timestamp': panel.tout.lastUpdate}

    def updateLVIVdata(self, interval, tab, lastUpdate):
        # Parse new data
        panel=self.panels[int(tab)]
        panel.parse()

        if type(lastUpdate)==dict and (lastUpdate['tab']==tab and lastUpdate['timestamp']==panel.lviv.lastUpdate): # no new data
            raise PreventUpdate

        # Prepare data
        p_lviv=panel.lviv.data.pb.unique()
        d_lviv=[]
        for pb in range(0,10):
            d_lviv.append({
                'VIN'       : panel.lviv.data[panel.lviv.data.pb==pb].VIN        if pb in p_lviv else [],
                'AMACDCDCIN': panel.lviv.data[panel.lviv.data.pb==pb].AMACDCDCIN if pb in p_lviv else []
                })

        return d_lviv, {'tab': tab, 'timestamp': panel.lviv.lastUpdate}

    def updateAMSlopedata(self, interval, tab, lastUpdate):
        # Parse new data
        panel=self.panels[int(tab)]
        panel.parse()

        if type(lastUpdate)==dict and (lastUpdate['tab']==tab and lastUpdate['timestamp']==panel.amsl.lastUpdate): # no new data
            raise PreventUpdate

        # Prepare data
        p_amsl=panel.amsl.data.pb.unique()
        d_amsl=[]
        for pb in range(0,10):
            d_amsl.append({
                'CALIN'  :panel.amsl.data[panel.amsl.data.pb==pb].CALIN   if pb in p_amsl else [],
                'AMACCAL':panel.amsl.data[panel.amsl.data.pb==pb].AMACCAL if pb in p_amsl else []
                })

        return d_amsl, {'tab': tab, 'timestamp': panel.amsl.lastUpdate}

    def updateDACdata(self, interval, tab, lastUpdate):
        # Parse new data
        panel=self.panels[int(tab)]
        panel.parse()

        lastUpdatePanel=max([panel.rshx.lastUpdate, panel.rshy.lastUpdate, panel.rcax.lastUpdate, panel.rcay.lastUpdate])
        if type(lastUpdate)==dict and (lastUpdate['tab']==tab
                                       and lastUpdate['timestamp']>=lastUpdatePanel
                                       ): # no new data
            raise PreventUpdate

        #
        # Empty dataframe with data
        p_rshx=panel.rshx.data.pb.unique()
        p_rshy=panel.rshy.data.pb.unique()
        p_rcax=panel.rcax.data.pb.unique()
        p_rcay=panel.rcay.data.pb.unique()
        d_ramp={'rshx':[], 'rshy':[], 'rcax':[], 'rcay':[]}
        for pb in range(0,10):
            pbdata=panel.rshx.data[panel.rshx.data.pb==pb]
            d_ramp['rshx'].append({col:pbdata[col] if pb in p_rshx else [] for col in pbdata.columns})

            pbdata=panel.rshy.data[panel.rshy.data.pb==pb]
            d_ramp['rshy'].append({col:pbdata[col] if pb in p_rshy else [] for col in pbdata.columns})

            pbdata=panel.rcax.data[panel.rcax.data.pb==pb]
            d_ramp['rcax'].append({col:pbdata[col] if pb in p_rcax else [] for col in pbdata.columns})

            pbdata=panel.rcay.data[panel.rcay.data.pb==pb]
            d_ramp['rcay'].append({col:pbdata[col] if pb in p_rcay else [] for col in pbdata.columns})

        return d_ramp, {'tab': tab, 'timestamp': lastUpdatePanel}

    def updateDCDCdata(self, interval, tab, lastUpdate):
        # Parse new data
        panel=self.panels[int(tab)]
        panel.parse()

        if type(lastUpdate)==dict and (lastUpdate['tab']==tab and lastUpdate['timestamp']==panel.dcdc.lastUpdate): # no new data        
            raise PreventUpdate

        # Graph data
        p_dcdc=panel.dcdc.data.pb.unique()
        d_dcdc=[]
        for pb in range(0,10):
            d_dcdc.append({
                'IOUT'      :panel.dcdc.data[panel.dcdc.data.pb==pb].IOUT       if pb in p_dcdc else [],
                'EFFICIENCY':panel.dcdc.data[panel.dcdc.data.pb==pb].EFFICIENCY if pb in p_dcdc else [],
                'AMACPTAT'  :panel.dcdc.data[panel.dcdc.data.pb==pb].AMACPTAT   if pb in p_dcdc else [],
                'AMACCUR10V':panel.dcdc.data[panel.dcdc.data.pb==pb].AMACCUR10V if pb in p_dcdc else [],
                'AMACCUR1V' :panel.dcdc.data[panel.dcdc.data.pb==pb].AMACCUR1V  if pb in p_dcdc else []
                })

        # DCDC efficiency at 2A
        dcdc2=panel.dcdc.data[panel.dcdc.data.IOUTSET==2].rename(columns={
            'EFFICIENCY':'value_EFF2A',
            'AMACPTAT':'value_AMACPTAT2A',
        })
        dcdc2['error_EFF2A']=dcdc2.value_EFF2A<0.6

        dcdc2=pad_dataframe_pbs(dcdc2)
        t_dcdc=dcdc2.to_dict('records')
        
        return t_dcdc, d_dcdc, {'tab': tab, 'timestamp': panel.dcdc.lastUpdate}

    def updateHVdata(self, interval, tab, lastUpdate):
        # Parse new data
        panel=self.panels[int(tab)]
        panel.parse()

        if type(lastUpdate)==dict and (lastUpdate['tab']==tab and lastUpdate['timestamp']==panel.hvrt.lastUpdate): # no new data
            raise PreventUpdate

        #
        # Graphs
        p_gains=panel.hvrt.data.pb.unique()
        p_hvrt=panel.hvrt.data.pb.unique()
        d_hvrt=[]
        for pb in range(0,10):
            d_hvrt.append({
                'HVIOUT'   :panel.hvrt.data[panel.hvrt.data.pb==pb].HVIOUT    if pb in p_hvrt else [],
                'AMACGAIN0':panel.hvrt.data[panel.hvrt.data.pb==pb].AMACGAIN0 if pb in p_hvrt else [],
                'AMACGAIN1':panel.hvrt.data[panel.hvrt.data.pb==pb].AMACGAIN1 if pb in p_hvrt else [],
                'AMACGAIN2':panel.hvrt.data[panel.hvrt.data.pb==pb].AMACGAIN2 if pb in p_hvrt else [],
                'AMACGAIN4':panel.hvrt.data[panel.hvrt.data.pb==pb].AMACGAIN4 if pb in p_hvrt else [],
                'AMACGAIN8':panel.hvrt.data[panel.hvrt.data.pb==pb].AMACGAIN8 if pb in p_hvrt else []
                })

        return d_hvrt, {'tab': tab, 'timestamp': panel.hvrt.lastUpdate}

    def runStartPBMonitor(self, button, tab, debug, save_csv, save_influx, *args):
        """
        Start powerboard temp monitor
        """
        if button==None:
            raise PreventUpdate

        boards =sum(args[0:10],[])

        if len(boards)==0:
            return 'No boards selected!'
         
        msg = ""
        options = []
        options+=['--board']
        options+=[','.join(map(str,boards))]
        if len(save_influx) > 0:
            options+=['-s','temperatures']
        if len(save_csv) > 0:
            options+=['-o', 'envMon']

        try:
            self.panels[int(tab)].runGeneral(program='pbv3_powerboard_climate', option=options, useConfig=True)
        except:
            return sys.exc_info()[0]
        
        return msg

    def runStopPBMonitor(self, button, tab):
        """
        Stop powerboard temp monitor
        """
        if button==None:
            raise PreventUpdate

        self.panels[int(tab)].stop()

        try:
            self.panels[int(tab)].runGeneral(program='powersupply', option=['-n','LV','power-off'], useConfig=True)
        except:
            return sys.exc_info()[0]

        return ''
    def updatePBTempGraph(self, interval, tab, add_NTCpb, add_PTAT, add_CTAT, add_chiller, *args):
        boards =sum(args[0:10],[])
        if len(boards) < 1:
            raise PreventUpdate

        panel=self.panels[int(tab)]
        panel.parse()
        get_msg = ''
        get_msg = ''.join(panel.data)

        if get_msg == "":
            raise PreventUpdate

        time=parseContent(get_msg, 'MONITOR_TIME_SECONDS')
        if time == "":
            raise PreventUpdate
        time_s = datetime.datetime.fromtimestamp(int(time))
        fig = go.Figure()

        for ib in range(len(boards)):
            temps_PB = parseContent(get_msg, 'Powerboard'+str(boards[ib]))
            if temps_PB == "":
                raise PreventUpdate

            if len(self.df_temp[int(tab)][boards[ib]].index) > 0:
                if time_s == self.df_temp[int(tab)][boards[ib]].iloc[-1]['time']:
                    raise PreventUpdate

            self.df_temp[int(tab)][boards[ib]].loc[len(self.df_temp[int(tab)][boards[ib]].index)] = [time_s, float(temps_PB.split(",")[2]), float(temps_PB.split(",")[3]), float(temps_PB.split(",")[4])]

            if len(add_NTCpb) > 0:
                fig.add_trace(go.Scatter(x=self.df_temp[int(tab)][boards[ib]]['time'], y=self.df_temp[int(tab)][boards[ib]]['NTCpb'], mode='lines', name='PB'+str(boards[ib])+"-NTCpb"))
            if len(add_PTAT) > 0:
                fig.add_trace(go.Scatter(x=self.df_temp[int(tab)][boards[ib]]['time'], y=self.df_temp[int(tab)][boards[ib]]['PTAT'], mode='lines', name='PB'+str(boards[ib])+"-PTAT"))
            if len(add_CTAT) > 0:
                fig.add_trace(go.Scatter(x=self.df_temp[int(tab)][boards[ib]]['time'], y=self.df_temp[int(tab)][boards[ib]]['CTAT'], mode='lines', name='PB'+str(boards[ib])+"-CTAT"))
            
        # add chiller_temp graph
        if len(add_chiller) > 0:
            if os.path.isdir("chillerMon"):
                filesAll = glob.glob("chillerMon/*.csv") 
                if len(filesAll) > 0:
                    file_chiller = sorted(filesAll, key = os.path.getmtime)[-1]
                    df_chiller_temp = pd.read_csv(file_chiller)
                    if len(df_chiller_temp.index) > 0:
                        fig.add_trace(go.Scatter(x=df_chiller_temp['time'], y=df_chiller_temp['temp'], mode='lines', name="chiller"))

        fig.update_layout(transition_duration=1000, xaxis_title="time", yaxis_title="temperature [C]", autosize=False, width=1000, height=500, margin=dict(l=100,r=50,b=50,t=50))
            
        return fig

    def runClearPBMonitor(self, button, tab):
        if button==None:
            raise PreventUpdate

        for pb in range(10):
            self.df_temp[int(tab)][pb] = self.df_temp[int(tab)][pb].iloc[0:0]

        return ''

    def run_background_task(self):
        commands_running = [[""]]*self.nTabs

        while True:
            print("=== This task runs in the background every 5 seconds...")
            sleep(5)

            ## first, check if there is new commands coming
            if self.command_dir == "":
                continue

            for tab in range(self.nTabs):
                cmd_file = self.command_dir+"/CMD_port"+str(self.port)+"_tab"+str(tab)+".txt"
                cmd_out_file = self.command_dir+"/OUT_CMD_port"+str(self.port)+"_tab"+str(tab)+".txt"
                # first, check if the running commands has finished or not
                if commands_running[tab][0] != "" and (not self.panels[tab].running):
                    if len(commands_running[tab]) == 1 or (len(commands_running[tab]) == 4 and len(commands_running[tab][3]) == 0): # all finished
                        print("=== Command in tab"+str(tab) + " has finished!!!")
                        with open(cmd_out_file, 'w') as file_out:
                            file_out.write(commands_running[tab][0]+";Done")

                        commands_running[tab] = [""]
                    else:
                        print("=== Testing the following pwb now")
                        args_this = commands_running[tab][3].pop()
                        print(args_this)
                        self.runAllTests_execute(tab, 0, commands_running[tab][2], None, args_this)
                    
                print("=== Searching for new commands for tab"+str(tab))
                if os.path.exists(cmd_file) and os.path.getsize(cmd_file) > 2:
                    command_this = ""
                    with open(cmd_file, 'r') as file_in:
                        command_this = file_in.readline().strip()
                    if ";" not in command_this:
                        continue
                    print("=== Received new command for port"+str(self.port)+", tab"+str(tab)+": "+command_this)
                    ## check if this tab is still running previous
                    command_splits = command_this.split(";")
                    command_id = command_splits[0]
                    command_type = command_splits[1]
                    if "stop" in command_type:
                        print("=== Stopping current job in tab"+str(tab))
                        self.stopRun_execute(tab)
                        self.panels[int(tab)].runGeneral(program='pbv3_powerOff', useConfig=True)
                        commands_running[tab] = [""]
                        os.remove(cmd_file)
                        sleep(2)
                        continue

                    if commands_running[tab][0] != "":
                        print("=== Tab"+str(tab)+" is still running other tasks, waiting for it to be finished...")
                        continue

                    else:
                        commands_running[tab] = [command_id]
                        args = []
                        args.append(command_splits[2]) # panel id
                        args.append(command_splits[3]) # version
                        args.append(int(command_splits[4])) # batch
                        boards = command_splits[5].split(",")
                        serials = command_splits[6].split(",")
                        for board in boards:
                            if board != "":
                                args.append([int(board)])
                            else:
                                args.append([])
                        for serial in serials:
                            if serial != "":
                                args.append(int(serial))
                            else:
                                args.append("")
                        pass1=hex_to_string(command_splits[7][2:])
                        pass2=hex_to_string(command_splits[8][2:])
                        # now actually sending the command
                        if "BasicTests" in command_type:
                            print("=== Get a command to run Basic Tests for tab"+str(tab))
                            self.runBasicTests_execute(tab, 0, "BasicTests", None, args)
                        if "AllTests" in command_type:
                            print("=== Get a command to run AllTests for tab"+str(tab))
                            # test the panel one by one
                            commands_running[tab].append("AllTests")
                            commands_running[tab].append(command_type.split(",")[1])
                            args_new_all = []
                            for ipb in range(10):
                                args_new = args[:3]
                                args_new += [[]] * 10
                                args_new += [""] * 10
                                if args[3+ipb] != []:
                                    args_new[3+ipb] = args[3+ipb]
                                    args_new[13+ipb] = args[13+ipb]
                                    args_new_all.append(args_new)
                            commands_running[tab].append(args_new_all)
                        if "uploadTestResults" in command_type:
                            print("=== Get a command to run uploadTestResults for tab"+str(tab))
                            self.uploadTestResults_execute(tab, pass1, pass2, args)
                        if "panelStageTransition" in command_type:
                            print("=== Get a command to run panelStageTransition for tab"+str(tab))
                            self.panelStageTransition_execute(command_type.split(",")[1], [True], tab, pass1, pass2, args)
                        if "BurnIn" in command_type:
                            print("===  Get a command to run BurnIn for tab"+str(tab))
                            load = command_type.split(",")[1]
                            time = command_type.split(",")[2]
                            self.runBurnIn_execute(tab, 0, load, time, args[3:13])
                        os.remove(cmd_file)
                        



