#!/usr/bin/env python

import argparse
import pandas as pd
import sys, os

from pbv3 import tests

parser = argparse.ArgumentParser(description='Convert PBv3 test result files from JSON to CSV.')
parser.add_argument('output',
                    help='path to output file')
parser.add_argument('input',nargs='+',
                    help='input glob of all json files to accumulate')
parser.add_argument('--testType','-t',default='STATUS',
                    help='test type to save')

args = parser.parse_args()

data=tests.load_testresults(args.input, testType=args.testType)
tests.apply_calibrations(data)
tests.apply_conversions(data)

data.to_csv(args.output)
