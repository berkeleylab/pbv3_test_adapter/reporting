#!/usr/bin/env python3
import os
import sys
import argparse
import time
import itkdb
import json
import getpass

if __name__ == '__main__':
    from __path__ import updatePath
    updatePath()

import itk_pdb.dbAccess as dbAccess
from itk_pdb.dbAccess import ITkPDSession

def getSerials(eFuse, lib):
  totalTime = []
  listPropertyTime = []
  getCompTime = []

  for i in range (0,50):
    pwbSN = []
    now = time.time()
    for ID in eFuse:
      try:
        # Get AMAC from eFuse Hex ID
        #production_database_scripts
        if lib=='p':
            now1 = time.time()
            listAmac = session.doSomething(action = "listComponentsByProperty", data = {'project':'S', 'componentType':'AMAC', 'propertyFilter':[{'code':'EFUSEID', 'operator':'=', 'value':ID}]}, method='POST')
            listPropertyTime.append(time.time()-now1)

        #itkdb with caching
        if lib=='c':
            now1 = time.time()
            listAmac = c.post('listComponentsByProperty', json = {'project':'S', 'componentType':'AMAC', 'propertyFilter':[{'code':'EFUSEID', 'operator':'=', 'value':ID}]})
            listPropertyTime.append(time.time()-now1)

        #itkdb without caching
        if lib=='i':
            now1 = time.time()
            listAmac = c.post('listComponentsByProperty', json = {'project':'S', 'componentType':'AMAC', 'propertyFilter':[{'code':'EFUSEID', 'operator':'=', 'value':ID}]}, headers={'Cache-Control':'no-cache'})
            listPropertyTime.append(time.time()-now1)

        # Get AMAC serial number
        for a in listAmac:
          amacSN = a['serialNumber']

        #Get AMAC
        #production_database_scripts
        if lib=='p':
            now2 = time.time()
            amac = session.doSomething(action = "getComponent", data = {'component':amacSN}, method='GET')
            getCompTime.append(time.time()-now2)

        #itkdb with caching
        if lib=='c':
            now2 = time.time()
            amac = c.get('getComponent', json = {'component':amacSN})
            getCompTime.append(time.time()-now2)

        #itkdb without caching
        if lib=='i':
            now2 = time.time()
            amac = c.get('getComponent', json = {'component':amacSN}, headers={'Cache-Control':'no-cache'})
            getCompTime.append(time.time()-now2)

        #Get powerboard that is the parent of the AMAC
        pwbSN.append(amac['parents'][0]['serialNumber'])
  
      except Exception as e:
        print(e)
        pwbSN.append("Error retrieving powerboard for hex ID " + ID)
  
    totalTime.append(time.time()-now)

  return [totalTime, listPropertyTime, getCompTime]

if __name__ == "__main__":
  if os.environ.get('ITK_DB_AUTH'):
      dbAccess.token = os.getenv('ITK_DB_AUTH')
        
  global session
  session = ITkPDSession()
  session.authenticate()

  if os.environ.get('ITKDB_ACCESS_CODE1') and os.environ.get('ITKDB_ACCESS_CODE2'):
      accessCode1 = os.environ.get('ITKDB_ACCESS_CODE1')
      accessCode2 = os.environ.get('ITKDB_ACCESS_CODE2')
  else:
      accessCode1 = getpass.getpass("AccessCode1: ")
      accessCode2 = getpass.getpass("AccessCode2: ")
  
  u = itkdb.core.User(accessCode1 = accessCode1, accessCode2 = accessCode2)
  c = itkdb.Client(user = u, expires_after=dict(days=1))
  c.user.authenticate()

  eFuse = ['0x0000581a', '0x00005816', '0x0000581b', '0x00005817', '0x0000581d', '0x00005818', '0x0000581e', '0x00005819']

  pds = getSerials(eFuse, 'p')
  ITKDB = getSerials(eFuse, 'i')
  ITKDBCa = getSerials(eFuse, 'c')

  data = {}
  data['pds']=pds
  data['ITKDB']=ITKDB
  data['ITKDBCa']=ITKDBCa

  with open('data/getPWBFromAMACeFuse.json', 'w+') as f:
      json.dump(data, f)
