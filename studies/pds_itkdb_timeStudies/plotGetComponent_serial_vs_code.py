#!/usr/bin/env python3

import os
import json
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from datetime import datetime
from dateutil import parser

with open('data/getComponent_serial_vs_code.json', 'r') as f:
    data = json.load(f)
    component = data['component']
    subtype = data['type']
    code = data['code']
    serial = data['serial']
    registration = data['registration']

x = [i for i in range(0,len(code))]
registration = [parser.isoparse(i) for i in registration]
registration = mpl.dates.date2num(registration)

plt.plot_date(registration, x)
plt.title('getComponet Call for COMPONENT: ' + component + ' SUBTYPE: ' + subtype)
plt.xlabel('Time Of Component Registration to Database')
plt.ylabel('Index of Component in List Returned by listComponents')
plt.show()
plt.savefig("data/getComponent_serial_vs_code_RegistIndex_" + component + "_" + subtype  + ".png")
plt.close()

plt.plot_date(registration, code)
plt.title('getComponet Call for COMPONENT: ' + component + ' SUBTYPE: ' + subtype)
plt.ylabel('Time [s]')
plt.xlabel('Time of component registration to database')
plt.show()
plt.savefig("data/getComponent_serial_vs_code_Regist_" + component + "_" + subtype + ".png")
plt.close()

bins = np.histogram(np.hstack((code, serial)), bins = 'auto')[1]
plt.hist(x=code, bins=bins, alpha=0.5, label='Component Code')
plt.hist(x=serial, bins=bins, alpha=0.5, label='Serial Number')
plt.title('getComponet Call for COMPONENT: ' + component + ' SUBTYPE: ' + subtype)
plt.xlabel('Time [s]')
plt.legend()
plt.show()
plt.savefig("data/getComponent_serial_vs_code_" + component + "_" + subtype + ".png")
plt.close()
