#!/usr/bin/env python3

import os
import json
import numpy as np
import matplotlib.pyplot as plt
from csv import reader

with open('data/getPWBFromAMACeFuse.json', 'r') as f:
    data = json.load(f)

    totalTime = data['pds'][0]
    totalTimeITKDB = data['ITKDB'][0]
    totalTimeITKDBCa = data['ITKDBCa'][0]

    listPropertyTime = data['pds'][1]
    listPropertyTimeITKDB = data['ITKDB'][1]
    listPropertyTimeITKDBCa = data['ITKDBCa'][1]

    getCompTime = data['pds'][2]
    getCompTimeITKDB = data['ITKDB'][2]
    getCompTimeITKDBCa = data['ITKDBCa'][2]

    bins = np.histogram(np.hstack((totalTime, totalTimeITKDB, totalTimeITKDBCa)), bins = 'auto')[1]
    plt.hist(x=totalTime, bins=bins, alpha=0.5, label = 'production_database_scripts: Mean ' + str(round(sum(totalTime)/len(totalTime),2)))
    plt.hist(x=totalTimeITKDB, bins=bins, alpha=0.5, label = 'itkdb: Mean ' + str(round(sum(totalTimeITKDB)/len(totalTimeITKDB),2)))
    plt.hist(x=totalTimeITKDBCa, bins=bins, alpha=0.5, label = 'itkdb Cached: Mean ' + str(round(sum(totalTimeITKDBCa)/len(totalTimeITKDBCa),2)))
    plt.title('Total Time to Run getPWBFromAMACeFuse.py')
    plt.xlabel('Time [s]')
    plt.legend()
    plt.show()
    plt.savefig("data/getPWBFromAMACeFuse_Total.png")
    plt.close()
  
    bins = np.histogram(np.hstack((listPropertyTime, listPropertyTimeITKDB, listPropertyTimeITKDBCa)), bins = 'auto')[1]
    plt.hist(x=listPropertyTime, bins=bins, alpha = 0.5, label='production_database_scripts: Mean ' + str(round(sum(listPropertyTime)/len(listPropertyTime),2)))
    plt.hist(x=listPropertyTimeITKDB, bins=bins, alpha = 0.5, label='itkdb: Mean ' + str(round(sum(listPropertyTimeITKDB)/len(listPropertyTimeITKDB),2)))
    plt.hist(x=listPropertyTimeITKDBCa, bins=bins, alpha = 0.5, label='itkdb Cached: Mean ' + str(round(sum(listPropertyTimeITKDBCa)/len(listPropertyTimeITKDBCa),2)))
    plt.title('Total Time of Single Call to listComponentsByProperty')
    plt.xlabel('Time [s]')
    plt.legend()
    plt.show()
    plt.savefig("data/getPWBFromAMACeFuse_listComponents.png")
    plt.close()
  
    bins = np.histogram(np.hstack((getCompTime, getCompTimeITKDB, getCompTimeITKDBCa)), bins = 'auto')[1]
    plt.hist(x=getCompTime, bins=bins, alpha=0.5, label='production_database_scripts: Mean ' + str(round(sum(getCompTime)/len(getCompTime),2)))
    plt.hist(x=getCompTimeITKDB, bins=bins, alpha=0.5, label='itkdb: Mean ' + str(round(sum(getCompTimeITKDB)/len(getCompTimeITKDB),2)))
    plt.hist(x=getCompTimeITKDBCa, bins=bins, alpha=0.5, label='itkdb Cached: Mean ' + str(round(sum(getCompTimeITKDBCa)/len(getCompTimeITKDBCa),2)))
    plt.title('Total Time of Single Call to getComponent')
    plt.xlabel('Time [s]')
    plt.legend()
    plt.show()
    plt.savefig("data/getPWBFromAMACeFuse_getComponent.png")
    plt.close()
